---
- name: Add PostgreSQL repository key
  apt_key: url=https://www.postgresql.org/media/keys/ACCC4CF8.asc

- name: Add PostgreSQL repository
  apt_repository: repo='deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main'

- name: Install PGDG keyring
  apt: pkg=pgdg-keyring state=latest

- name: Install PostgreSQL
  apt: name={{ item }}
  with_items:
    - postgresql-{{ pg_version }}
    - postgresql-client-{{ pg_version }}
    - postgresql-contrib-{{ pg_version }}
    - libpq-dev
    - python-psycopg2
  register: db_setup

- name: Grant local user admin access to PostgreSQL
  lineinfile:
    dest="/etc/postgresql/{{ pg_version }}/main/pg_hba.conf"
    insertafter="^#\sIPv4\slocal.+" line="local   all             {{ admin_user }}                                 ident"
  notify: restart postgresql

- name: Ensure PostgreSQL server is started
  service: name=postgresql state=started enabled=yes

- name: Wait for PostgreSQL to start
  wait_for: port=5432

- name: Ensure database is created
  sudo: yes
  sudo_user: postgres
  postgresql_db: name={{ db_name }}
                 encoding='UTF-8'
                 lc_collate='fi_FI.UTF-8'
                 lc_ctype='fi_FI.UTF-8'

- name: Ensure admin user has access to database
  sudo: yes
  sudo_user: postgres
  postgresql_user: name="{{ admin_user }}" role_attr_flags=SUPERUSER,LOGIN

- name: Ensure user has access to database
  sudo: yes
  sudo_user: postgres
  postgresql_user: db={{ db_name }} name={{ db_username }} password={{ db_password }} priv=ALL

- name: Ensure user does not have unnecessary privileges
  sudo: yes
  sudo_user: postgres
  postgresql_user: name={{ db_username }} role_attr_flags=NOSUPERUSER,NOCREATEDB

- name: Install Hstore PostgreSQL extension
  postgresql_ext: name=hstore db=goldie
  sudo: yes
  sudo_user: postgres

- name: Grant Vagrant user admin access to PostgreSQL
  lineinfile:
    dest="/etc/postgresql/{{ pg_version }}/main/pg_hba.conf"
    insertafter="^#\sIPv4\slocal.+" line="local   all             {{ admin_user }}                                 ident"

- name: Ensure Vagrant user has access to database
  sudo: yes
  sudo_user: postgres
  postgresql_user: name={{ admin_user }} role_attr_flags=SUPERUSER
