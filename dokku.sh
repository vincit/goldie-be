#!/bin/sh

java -Xms384m -Xmx512m  \
 -Ddb.url=jdbc:postgresql://$DATABASE_HOST:$DATABASE_PORT/$DATABASE_DATABASE?stringtype=unspecified \
 -Ddb.username=$DATABASE_USERNAME \
 -Ddb.password=$DATABASE_PASSWORD \
 -jar target/dependency/webapp-runner.jar \
 --context-xml ./tomcat-context.xml \
 --uri-encoding UTF-8 \
 --force-https \
 --port $PORT target/*.war
