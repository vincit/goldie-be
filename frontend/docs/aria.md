# Aria related material
--------------

### Aria documentation and guidelines
[http://rawgit.com/w3c/aria-in-html/master/index.html](http://rawgit.com/w3c/aria-in-html/master/index.html)

### Aria attributes
[http://w3c.github.io/html-reference/aria/aria.html](http://w3c.github.io/html-reference/aria/aria.html)

### Using aria attributes
[https://specs.webplatform.org/html-aria/webspecs/master/#document-conformance-requirements-for-use-of-aria-attributes-in-html](https://specs.webplatform.org/html-aria/webspecs/master/#document-conformance-requirements-for-use-of-aria-attributes-in-html)
