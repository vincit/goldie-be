# Redux help
------------

This is an example on how to create a reducer with actions and a saga to get data from the backend.

### Actions

To create a Redux action, we are using redux-actions library, that outputs actions as flux standard actions. ([https://github.com/acdlite/redux-actions](https://github.com/acdlite/redux-actions)).

To create a new action, somewhere in your code:

```
import { createAction } from 'redux-actions';

export const requestUserData = createAction('REQUEST_USER_DATA');
export const requestUserDataSuccess = createAction('REQUEST_USER_DATA_SUCCESS');
export const requestUserDataFailure = createAction('REQUEST_USER_DATA_FAILURE');
```

This will create redux actions. The first action is used to create the action that will dispatch the API request itself. The success and failure actions are dispatched based on the API requests result. Without redux-actions, you would create a corresponding action manually like this:

```
export const requestUserData = (userData) => ({
    type: REQUEST_USER_DATA,
    payload: userData
});
```

### Reducers

The redux-actions library also handles actions through `handleAction` and `handleActions` functions. To create a reducer for the user-data. We can say:

```
export const userDataReducer = handleActions({
  [requestUserDataSuccess]: (state, action) => Immutable.List(action.payload.content),
  [requestUserDataFailure]: () => {
    alert('Käyttäjätietojen haku epäonnistui'); // eslint-disable-line
    return Immutable.List(initialUserDataState);
  }
}, Immutable.List(initialUserDataState));
```

When a `requestUserDataSuccess` action is dispatched, the `[requestUserDataSuccess]` will be accessed and it will create a new `Immutable.List` from the actions payload. Also, when the request fails, the reducer will return the initial (empty) state. The last parameter for `handleActions` is the initial state of the reducer.

### Sagas

TODO
