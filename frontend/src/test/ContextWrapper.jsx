import React, { PropTypes } from 'react';

/**
 * Wrap a component in router context for testing purposes.
 * <ContextWrapper><YourOwnComponent {...props} /></ContextWrapper>
 * Reference: https://github.com/reactjs/react-router/blob/0.13.x/docs/guides/testing.md
 */
class ContextWrapper extends React.Component {

  getChildContext() {
    return {
      i18n: {
        getFixedT () {},
        loadNamespaces () {},
        t () {}
      },
      router: {
        createHref () {},
        makePath () {},
        makeHref () {},
        transitionTo () {},
        replaceWith () {},
        goBack () {},
        push () {},
        replace () {},
        go () {},
        goForward () {},
        setRouteLeaveHook () {},
        getCurrentPath () {},
        getCurrentRoutes () {},
        getCurrentPathname () {},
        getCurrentParams () {},
        getCurrentQuery () {},
        isActive () {},
        getRouteAtDepth() {},
        setRouteComponentAtDepth() {}
      }
    };
  }

  render() {
    return this.props.children;
  }
}

ContextWrapper.childContextTypes = {
  i18n: PropTypes.object,
  t: PropTypes.func,
  router: PropTypes.object
};

export default ContextWrapper;