import axios from 'axios';

const baseUrl = '/api/v1/admin';

const users = {
  fetchUserData: (payload) => {
    const queryString = `${baseUrl}/users?page=${payload.page}&size=${payload.size}&sort=${payload.sort}&type=${payload.type}`; // eslint-disable-line
    return axios.get(queryString);
  },
  fetchSingleUser: payload => axios.get(`${baseUrl}/users/${payload.id}`),
  updateUser: payload => axios.put(`${baseUrl}/users/${payload.id}`, payload),
  deleteUser: id => axios.delete(`${baseUrl}/users/${id}`)
};

export default users;
