import axios from 'axios';

const util = {
  fetchForgottenPassword: email => axios.post('/api/v1/password/forgot', email)
};

export default util;
