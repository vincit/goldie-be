import axios from 'axios';
import { browserHistory } from 'react-router';

const setupHttpInterceptors = () => {
  axios.interceptors.response.use(response => response,
      (error) => {
        switch (error.status) {
          case 401:
            browserHistory.push('signin');
            break;
          case 403:
            browserHistory.push('signin');
            break;
          default:
          // Show error page on all other errors?
        }

        return Promise.reject(error.response);
      }
  );
};

export default setupHttpInterceptors;
