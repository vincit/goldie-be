import { UserAuthWrapper } from 'redux-auth-wrapper';
import { redirectAndResetAuthentication } from '../views/login/actions';

const isUserAuthenticated = auth => auth.isAuthenticated === true;

const isUserAdmin = auth => isUserAuthenticated(auth)
  && auth.role === 'ROLE_ADMIN';

// From where in the redux state tree should the authentication information be fetched from
const getAuthSelector = state => state.getIn(['rootReducer', 'auth']).toJS();

// Basic check to see if the user is authenticated or not
export const userIsAuthenticated = UserAuthWrapper({
  authSelector: getAuthSelector,
  redirectAction: redirectAndResetAuthentication,
  failureRedirectPath: '/signin',
  wrapperDisplayName: 'UserIsAuthenticated',
  predicate: isUserAuthenticated,
  allowRedirectBack: false
});

// Checks if user has a role of ROLE_ADMIN
export const userIsAdmin = UserAuthWrapper({
  authSelector: getAuthSelector,
  redirectAction: redirectAndResetAuthentication,
  failureRedirectPath: '/signin',
  wrapperDisplayName: 'UserIsAdmin',
  predicate: isUserAdmin,
  allowRedirectBack: false
});
