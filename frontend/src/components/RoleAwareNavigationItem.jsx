import React from 'react';
import { RoleAwareComponent } from 'react-router-role-authorization';
import { NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class RoleAwareNavigationItem extends RoleAwareComponent {
  constructor(props) {
    super(props);

    this.allowedRoles = this.props.allowedRoles;
    this.userRoles = [this.props.userRole]; // Takes an array, now we only have one role per user
  }

  componentWillReceiveProps(nextProps) {
    this.userRoles = [nextProps.userRole]; // Update roles when new props come in
  }

  render() {
    const jsx = (
      <LinkContainer to={this.props.to}>
        <NavItem eventKey={this.props.eventKey}>{this.props.text}</NavItem>
      </LinkContainer>
    );

    return this.rolesMatched() ? jsx : null;
  }
}

export default RoleAwareNavigationItem;
