import React, { Component, PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { translate } from 'react-i18next';

import DeleteModal from './';

const styles = {
  general: {
    width: '50%',
    float: 'left'
  },
  button: {
    borderRadius: '0px',
    padding: '3px 12px',
    outline: 0,
    width: '100%'
  }
};

/* eslint-disable */
class DeleteModalButton extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };
  }

  closeModal() {
    this.setState({
      isModalOpen: false
    });
  }

  openModal() {
    this.setState({ isModalOpen: true });
  }

  render() {
    const { t } = this.props;

    return (
      <div style={styles.general}>
        <Button
          bsStyle="danger"
          style={styles.button}
          onClick={() => this.openModal()}
        >
          {t('deletemodal.openingButtonText')}
        </Button>
        <DeleteModal
          modalData={this.props.modalData}
          show={this.state.isModalOpen}
          onHide={() => { this.closeModal()}}
          onSuccess={() => this.closeModal()}
        />
      </div>
    );
  }
}

DeleteModalButton.propTypes = {
  t: PropTypes.func.isRequired,
  modalData: PropTypes.object.isRequired
};

export default translate(['modals'], { wait: true })(DeleteModalButton);
