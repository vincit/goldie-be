import React, { PropTypes } from 'react';
import { Modal } from 'react-bootstrap';
import { translate } from 'react-i18next';

import EditUserForm from '../../Forms/EditUserForm';

/* eslint-disable */
const EditUserModal = ({ show, onHide, onSuccess, t, updateUser }) => (
  <Modal dialogClassName="editusermodal" show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>{t('editusermodal.title')}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <EditUserForm updateUser={(data) => { updateUser(data, onSuccess) }} hideModal={onHide} />
    </Modal.Body>
  </Modal>
);

EditUserModal.propTypes = {
  onHide: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired
};

export default translate(['modals'], { wait: true })(EditUserModal);
