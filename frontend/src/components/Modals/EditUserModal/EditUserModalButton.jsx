import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import { translate } from 'react-i18next';

import { setCurrentUser } from '../../../views/users/actions';
import EditUserModal from './';

const styles = {
  general: {
    width: '50%',
    float: 'left'
  },
  button: {
    borderRadius: '0px',
    padding: '3px 12px',
    outline: 0,
    width: '100%'
  }
};

/* eslint-disable */
class EditUserModalButton extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };
  }

  closeModal() {
    this.setState({
      isModalOpen: false
    });
  }

  openModal() {
    this.props.setCurrentUser(this.props.userData);
    this.setState({ isModalOpen: true });
  }

  render() {
    const { t } = this.props;

    return (
      <div style={styles.general}>
        <Button
          bsStyle="primary"
          style={styles.button}
          onClick={() => this.openModal()}
        >
          {t('editusermodal.openingButtonText')}
        </Button>
        <EditUserModal
          show={this.state.isModalOpen}
          onHide={() => { this.closeModal()}}
          onSuccess={() => this.closeModal()}
        />
      </div>
    );
  }
}

EditUserModalButton.propTypes = {
  t: PropTypes.func.isRequired,
  userData: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => ({
  setCurrentUser(userData) {
    dispatch(setCurrentUser(userData));
  }
});

const connectedComponent = connect(
  state => state.toJS(),
  mapDispatchToProps
)(EditUserModalButton);

export default translate(['modals'], { wait: true })(connectedComponent);
