import { connect } from 'react-redux';
import { requestUpdateUser } from '../../../views/users/actions';

import EditUserModal from './EditUserModal';

const mapStateToProps = state => state.toJS();

const mapDispatchToProps = dispatch => ({
  updateUser(userData, onSuccess) {
    dispatch(requestUpdateUser({
      userData: userData.toJS(),
      onSuccess
    }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditUserModal);
