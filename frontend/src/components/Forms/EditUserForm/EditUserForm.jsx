import React, { PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import { translate } from 'react-i18next';
import { Field, reduxForm } from 'redux-form/immutable';

import InputField from './../Fields/InputField';
import validateForm from './validateForm';

const buttonStyles = {
  general: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  submitBtn: {
    marginRight: '10px'
  }
};

/* eslint-disable */
const EditUserForm = ({ handleSubmit, submitting, t, updateUser, hideModal }) => (
  <form onSubmit={handleSubmit(updateUser)}>
    <Field name="username" type="text" component={InputField} placeholder={t('editUserForm.usernamePlaceholder')} />
    <Field name="role" type="text" component={InputField} placeholder={t('editUserForm.rolePlaceholder')} disabled />
    <Field name="email" type="email" component={InputField} placeholder={t('editUserForm.emailPlaceholder')} />
    <Field name="firstName" type="text" component={InputField} placeholder={t('editUserForm.firstNamePlaceholder')} />
    <Field name="lastName" type="text" component={InputField} placeholder={t('editUserForm.lastNamePlaceholder')} />
    <div style={buttonStyles.general}>
      <Button style={buttonStyles.submitBtn} bsStyle="primary" type="submit" disabled={submitting}>{t('buttons.submit')}</Button>
      <Button type="button" onClick={hideModal}>{t('buttons.cancel')}</Button>
    </div>
  </form>
);

EditUserForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  updateUser: PropTypes.func.isRequired
};

const form = reduxForm({
  form: 'EditUserForm',
  validate: validateForm
})(EditUserForm);

export default translate(['forms'], { wait: true })(form);
