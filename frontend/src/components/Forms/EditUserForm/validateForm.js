import i18next from 'i18next';

const validateForm = (values) => {
  const errors = {};

  if (!values.get('username')) {
    errors.username = i18next.t('forms:validationErrors.required');
  }

  if (!values.get('role')) {
    errors.role = i18next.t('forms:validationErrors.required');
  }

  if (!values.get('email')) {
    errors.email = i18next.t('forms:validationErrors.required');
  }

  if (!values.get('firstName')) {
    errors.firstName = i18next.t('forms:validationErrors.required');
  }

  if (!values.get('lastName')) {
    errors.lastName = i18next.t('forms:validationErrors.required');
  }

  return errors;
};

export default validateForm;
