/* eslint-env node, mocha */
/* eslint no-console: 0*/

import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

import ContextWrapper from '../../../test/ContextWrapper';

import { PureLoginForm } from './index';

describe('LoginForm component', () => {
  let props = {};
  let component = null;

  beforeEach(() => {
    props = {
      onSubmit: sinon.spy(),
      t: sinon.spy()
    };
    component = mount(<ContextWrapper><PureLoginForm {...props} /></ContextWrapper>);
  });

  it('should call the props login function with correct values', () => {
    component.find('#username').get(0).value = 'John Doe';
    component.find('#password').get(0).value = 'Secret password';
    component.find('input[type="checkbox"]').get(0).checked = true;
    component.find('form').simulate('submit');
    expect(props.onSubmit.calledWithMatch({
      username: 'John Doe',
      password: 'Secret password',
      rememberMe: true }))
      .to.equal(true);
  });
});
