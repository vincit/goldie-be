/* eslint-env node, mocha */
/* eslint no-console: 0*/

import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import ContextWrapper from '../../../test/ContextWrapper';

import { PureForgotPasswordForm } from './index';

describe('ForgotPasswordForm component', () => {
  let props = {};
  let component = null;

  beforeEach(() => {
    props = {
      onSubmit: sinon.spy(),
      t: sinon.spy()
    };
    component = mount(<ContextWrapper><PureForgotPasswordForm {...props} /></ContextWrapper>);
  });

  it('should call the props onSubmit function with correct values', () => {
    component.find('#email').get(0).value = 'test@test.com';
    component.find('.forgotpasswordform').simulate('submit');
    expect(props.onSubmit.calledWithMatch({
      email: 'test@test.com'
    })).to.equal(true);
  });
});
