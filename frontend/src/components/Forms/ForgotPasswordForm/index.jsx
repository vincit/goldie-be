/* eslint-disable */
import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Button, ControlLabel, FormControl, FormGroup } from 'react-bootstrap';
import { translate } from 'react-i18next';

const styles = {
  paddingBottom: '150px'
};

class ForgotPasswordForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isSendRequestBtnDisabled: true
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    const email = ReactDOM.findDOMNode(this.refs.email).value;
    this.props.onSubmit({ email });
  }

  handleChange() {
    const email = ReactDOM.findDOMNode(this.refs.email).value;

    email.length > 0 ? // eslint-disable-line
      this.setState({ isSendRequestBtnDisabled: false }) :
      this.setState({ isSendRequestBtnDisabled: true });
  }

  render() {
    const { t } = this.props;

    return (
      <form className="forgotpasswordform" style={styles} onSubmit={e => this.handleSubmit(e)}>
        <FormGroup controlId="email">
          <ControlLabel>{t('email')}</ControlLabel>
          <FormControl
            type="email"
            placeholder={t('emailPlaceholder')}
            ref="email"
            onChange={() => this.handleChange()}
          />
        </FormGroup>
        <Button
          bsStyle="primary"
          className="btn-block"
          type="submit"
          disabled={this.state.isSendRequestBtnDisabled}
        >
          {t('btnText')}
        </Button>
      </form>
    );
  }
}

ForgotPasswordForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default translate(['forgotpassword'], { wait: true })(ForgotPasswordForm);
export { ForgotPasswordForm as PureForgotPasswordForm };
