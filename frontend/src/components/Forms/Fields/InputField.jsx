import React, { PropTypes } from 'react';
import { ControlLabel, FormControl, HelpBlock, FormGroup } from 'react-bootstrap';

const InputField = field => (
  <FormGroup
    controlId={field.input.placeholder}
    validationState={(field.invalid && field.error) ? 'error' : 'success'}
  >
    <ControlLabel>{field.input.placeholder}</ControlLabel>
    <FormControl {...field.input} />
    {
      (field.invalid && field.error) ?
        <HelpBlock>{field.error}</HelpBlock> : null
    }
  </FormGroup>
);

InputField.propTypes = {
  field: PropTypes.object.isRequired
};

export default InputField;
