import React, { PropTypes } from 'react';
import { Alert } from 'react-bootstrap';

const SuccessMessage = ({ isVisible, children }) => {
  if (isVisible) {
    return (
      <Alert bsStyle="success">
        {children}
      </Alert>
    );
  }
  return null;
};

SuccessMessage.propTypes = {
  isVisible: PropTypes.bool,
  children: PropTypes.node.isRequired
};

export default SuccessMessage;
