import React, { PropTypes } from 'react';
import { Col, Row } from 'react-bootstrap';

const styles = {
  height: '100%',
  display: 'flex',
  alignItems: 'center'
};

const CenteredContent = ({ children, className }) => (
  <Row className={className} style={styles}>
    <Col xs={4} xsOffset={4}>
      {children}
    </Col>
  </Row>
);

CenteredContent.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

export default CenteredContent;
