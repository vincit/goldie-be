import React, { PropTypes } from 'react';
import { Table, Column } from 'fixed-data-table';

import TextCell from '../../../components/Tables/Cells/TextCell';
import HeaderCell from '../../../components/Tables/Cells/HeaderCell';
import UserActionsButtonCell from '../../../components/Tables/Cells/UserActionsButtonCell';

const UsersTable = ({ users, width }) => {
  const height = (users.length * 50) + 52; // Padding to count for the header

  // If there are no users (yet..) don't render anything
  if (users.length === 0) {
    return null;
  }

  return (
    <Table
      rowsCount={users.length}
      rowHeight={50}
      headerHeight={50}
      width={width}
      height={height}
    >
      <Column
        header={<HeaderCell translationKey="userstable.headers.username" />}
        cell={<TextCell data={users} col="username" />}
        flexGrow={1}
        width={100}
      />
      <Column
        header={<HeaderCell translationKey="userstable.headers.role" />}
        cell={<TextCell data={users} col="role" />}
        flexGrow={1}
        width={100}
      />
      <Column
        header={<HeaderCell translationKey="userstable.headers.email" />}
        cell={<TextCell data={users} col="email" />}
        flexGrow={1}
        width={100}
      />
      <Column
        header={<HeaderCell translationKey="userstable.headers.firstName" />}
        cell={<TextCell data={users} col="firstName" />}
        flexGrow={1}
        width={100}
      />
      <Column
        header={<HeaderCell translationKey="userstable.headers.lastName" />}
        cell={<TextCell data={users} col="lastName" />}
        flexGrow={1}
        width={100}
      />
      <Column
        cell={<UserActionsButtonCell
          data={users}
        />}
        flexGrow={0}
        width={200}
      />
    </Table>
  );
};

UsersTable.propTypes = {
  width: PropTypes.number.isRequired,
  users: PropTypes.array.isRequired
};

export default UsersTable;
