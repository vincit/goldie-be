import { connect } from 'react-redux';
import UsersTable from './UsersTable';

import {
  requestDeleteUser,
  setCurrentUser
} from '../../../views/users/actions';

const mapStateToProps = (state, ownProps) => ({
  users: ownProps.users.toJS()
});

const mapDispatchToProps = dispatch => ({
  deleteUser(payload) {
    dispatch(requestDeleteUser(payload));
  },
  setCurrentUser(userData) {
    dispatch(setCurrentUser(userData));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersTable);
