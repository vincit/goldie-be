import React from 'react';

const Home = () => (
  <div>
    <h2>Goldie Backend with React</h2>
    <p>
      Home page
    </p>
  </div>
);

export default Home;
