import { connect } from 'react-redux';
import Home from './Home';

const mapStateToProps = state => state.toJS();

export default connect(
  mapStateToProps
)(Home);
