import Immutable from 'immutable';
import { handleActions } from 'redux-actions';
import {
  forgotPasswordRequestSuccess,
  forgotPasswordRequestFailure,
  resetValidationState
} from './actions';

const initialState = {
  forgotPasswordRequestError: false,
  forgotPasswordRequestSuccess: false
};

const forgotPasswordReducer = handleActions({
  [forgotPasswordRequestSuccess]: () => Immutable.Map(initialState).merge({
    forgotPasswordRequestSuccess: true
  }),
  [forgotPasswordRequestFailure]: () => Immutable.Map(initialState).merge({
    forgotPasswordRequestError: true
  }),
  [resetValidationState]: () => Immutable.Map(initialState)
}, Immutable.Map(initialState));

export default forgotPasswordReducer;
