import { connect } from 'react-redux';
import ForgotPassword from './ForgotPassword';

import './forgotpassword.scss';

import {
  requestForgotPassword,
  resetValidationState
} from './actions';

const mapStateToProps = state => ({
  error: state.getIn(['rootReducer', 'forgotPassword', 'forgotPasswordRequestError']),
  success: state.getIn(['rootReducer', 'forgotPassword', 'forgotPasswordRequestSuccess'])
});

const mapDispatchToProps = dispatch => ({
  requestForgottenPassword(email) {
    dispatch(requestForgotPassword(email));
  },
  resetValidationState() {
    dispatch(resetValidationState());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword);
