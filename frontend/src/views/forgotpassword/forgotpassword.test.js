/* eslint-env node, mocha */
/* eslint no-console: 0 */

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

import { PureForgetPassword } from './ForgotPassword';

describe('ForgotPassword component', () => {
  let props = {};

  beforeEach(() => {
    props = {
      requestForgottenPassword: sinon.spy(),
      resetValidationState: sinon.spy(),
      t: sinon.spy(),
      error: false,
      success: true
    };
    shallow(<PureForgetPassword {...props} />);
  });

  it('should call resetValidationState on init', () => {
    expect(props.resetValidationState.calledOnce).to.equal(true);
  });
});
