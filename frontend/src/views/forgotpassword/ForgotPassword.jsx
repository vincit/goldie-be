import React, { Component, PropTypes } from 'react';
import { translate } from 'react-i18next';

import ErrorMessage from '../../components/Messages/ErrorMessage';
import SuccessMessage from '../../components/Messages/SuccessMessage';
import ForgotPasswordForm from '../../components/Forms/ForgotPasswordForm';
import CenteredContent from '../../components/CenteredContent';

class ForgotPassword extends Component {

  constructor(props) { // eslint-disable-line
    super(props);
  }

  componentWillMount() {
    this.props.resetValidationState();
  }

  render() {
    const { t } = this.props;

    return (
      <CenteredContent className="forgottenpassword">
        <ErrorMessage isVisible={this.props.error}>
          <strong>{t('validation.error_1')}</strong>
          <p>{t('validation.error_2')}</p>
        </ErrorMessage>
        <SuccessMessage isVisible={this.props.success}>
          <strong>{t('validation.success_1')}</strong>
          <p>{t('validation.success_2')}</p>
        </SuccessMessage>
        <h3 className="heading">{t('title')}</h3>
        <ForgotPasswordForm
          onSubmit={(formData) => { this.props.requestForgottenPassword(formData) }}
        />
      </CenteredContent>
    );
  }
}

ForgotPassword.propTypes = {
  requestForgottenPassword: PropTypes.func.isRequired,
  resetValidationState: PropTypes.func.isRequired,
  error: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  t: PropTypes.func.isRequired
};

export default translate(['forgotpassword'], { wait: true })(ForgotPassword);
export { ForgotPassword as PureForgetPassword };
