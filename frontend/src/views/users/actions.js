import { createAction } from 'redux-actions';

export const requestUserData = createAction('REQUEST_USER_DATA');
export const requestUserDataSuccess = createAction('REQUEST_USER_DATA_SUCCESS');
export const requestUserDataFailure = createAction('REQUEST_USER_DATA_FAILURE');

export const requestSingleUser = createAction('REQUEST_SINGLE_USER');
export const requestSingleUserSuccess = createAction('REQUEST_SINGLE_USER_SUCCESS');
export const requestSingleUserFailure = createAction('REQUEST_SINGLE_USER_FAILURE');

export const requestUpdateUser = createAction('REQUEST_UPDATE_USER');
export const requestUpdateUserSuccess = createAction('REQUEST_UPDATE_USER_SUCCESS');
export const requestUpdateUserFailure = createAction('REQUEST_UPDATE_USER_FAILURE');

export const requestDeleteUser = createAction('REQUEST_DELETE_USER');
export const requestDeleteUserSuccess = createAction('REQUEST_DELETE_USER_SUCCESS');
export const requestDeleteUserFailure = createAction('REQUEST_DELETE_USER_FAILURE');

export const setCurrentUser = createAction('SET_CURRENT_USER');
