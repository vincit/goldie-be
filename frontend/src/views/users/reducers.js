/* global _ */
import { combineReducers } from 'redux-immutable';
import * as _ from 'lodash';
import Immutable from 'immutable';
import { handleActions } from 'redux-actions';
import {
  requestUserDataSuccess,
  requestUserDataFailure,
  requestSingleUserSuccess,
  requestSingleUserFailure,
  requestUpdateUserSuccess,
  requestUpdateUserFailure,
  requestDeleteUserSuccess,
  requestDeleteUserFailure,
  setCurrentUser
} from './actions';

const initialUserDataState = [];

export const userDataReducer = handleActions({
  [requestUserDataSuccess]: (state, action) => Immutable.List(action.payload.content),
  [requestUserDataFailure]: () => {
    alert('Käyttäjätietojen haku epäonnistui'); // eslint-disable-line
    return Immutable.List(initialUserDataState);
  },
  [requestDeleteUserSuccess]: (state, action) => {
    const foundUserIndex = state.findIndex(user => user.id === action.payload.id);
    return state.delete(foundUserIndex);
  },
  [requestDeleteUserFailure]: (state) => {
    alert('Käyttäjän poistaminen epäonnistui'); // eslint-disable-line
    return state;
  },
  [requestUpdateUserSuccess]: (state, action) => {
    const foundUserIndex = state.findIndex(user => user.id === action.payload.id);
    return state.update(foundUserIndex, () => action.payload);
  },
  [requestUpdateUserFailure]: (state, action) => {
    const validationErrors = action.payload.validationErrors ?
      _.map(action.payload.validationErrors, 'errorMessage') : '';
    alert(`Käyttäjän päivittäminen epäonnistui ${JSON.stringify(validationErrors)}`); // eslint-disable-line
    return state;
  }
}, Immutable.List(initialUserDataState));

const initialCurrentUserState = [];

export const currentUserReducer = handleActions({
  [requestSingleUserSuccess]: (state, action) => Immutable.Map(action.payload),
  [requestSingleUserFailure]: () => {
    alert('Käyttäjän haku epäonnistui'); // eslint-disable-line
    return Immutable.Map(initialCurrentUserState);
  },
  [setCurrentUser]: (state, action) => Immutable.Map(action.payload)
}, Immutable.Map(initialCurrentUserState));

export default combineReducers({
  users: userDataReducer,
  currentUser: currentUserReducer
});
