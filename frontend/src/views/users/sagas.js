/* global _ */
import { isFunction } from 'lodash';
import { takeEvery } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import { createGenerator } from '../../util/helpers';
import {
  requestUserData,
  requestUserDataSuccess,
  requestUserDataFailure,
  requestSingleUser,
  requestSingleUserSuccess,
  requestSingleUserFailure,
  requestUpdateUser,
  requestUpdateUserSuccess,
  requestUpdateUserFailure,
  requestDeleteUser,
  requestDeleteUserSuccess,
  requestDeleteUserFailure
} from './actions';
import Api from '../../api';

export const requestUsers = createGenerator(
  Api.users.fetchUserData, requestUserDataSuccess, requestUserDataFailure
);

export const requestUser = createGenerator(
  Api.users.fetchSingleUser, requestSingleUserSuccess, requestSingleUserFailure
);

export function* requestUpdateSingleUser({ payload }) {
  try {
    const response = yield call(Api.users.updateUser, payload.userData);
    yield put(requestUpdateUserSuccess(response.data));
    // Pass onSuccess callback here and if request is successful,
    // call the function (close the editing modal for example..).
    // Not sure if this is the correct way.
    if (isFunction(payload.onSuccess)) {
      yield call(payload.onSuccess);
    }
  } catch (err) {
    yield put(requestUpdateUserFailure(err.data));
  }
}

export function* requestDeleteSingleUser({ payload }) {
  try {
    yield call(Api.users.deleteUser, payload.id);
    yield put(requestDeleteUserSuccess({ id: payload.id }));
    // Pass onSuccess callback here and if request is successful,
    // call the function (close the editing modal for example..).
    // Not sure if this is the correct way.
    if (_.isFunction(payload.onSuccess)) {
      yield call(payload.onSuccess);
    }
  } catch (err) {
    yield put(requestDeleteUserFailure(err));
  }
}

const watchRequestUserData = takeEvery(requestUserData().type, requestUsers);
const watchRequestSingleUser = takeEvery(requestSingleUser().type, requestUser);
const watchRequestUpdateUser = takeEvery(requestUpdateUser().type, requestUpdateSingleUser);
const watchRequestDeleteUser = takeEvery(requestDeleteUser().type, requestDeleteSingleUser);

// Export as an array so it cleaner to handle in the root saga.
export default [
  watchRequestUserData,
  watchRequestSingleUser,
  watchRequestUpdateUser,
  watchRequestDeleteUser
];
