import React from 'react';
import { Route } from 'react-router';
import { userIsAuthenticated, userIsAdmin } from '../../../routes/route.authentication';
import Users from '..';

const UsersRoutes = (
  <Route path="users" component={userIsAdmin(userIsAuthenticated(Users))} />
);

export default UsersRoutes;
