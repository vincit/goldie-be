import { connect } from 'react-redux';
import App from './app';

import { authenticateUser } from '../../views/login/actions';

const mapStateToProps = state => state.toJS();

const mapDispatchToProps = dispatch => ({
  authenticate() {
    dispatch(authenticateUser());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
