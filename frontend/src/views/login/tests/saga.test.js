/* eslint-env node, mocha */
/* eslint no-console: 0*/

import { call, put } from 'redux-saga/effects';
import { expect } from 'chai';

import { authenticate, login, logout, redirectAndReset } from '../sagas';
import {
  resetAuthenticationState,
  loginSuccess,
  loginFailure,
  logoutSuccess
} from '../actions';
import Api from '../../../api';

describe('authentication saga', () => {
  describe('on login, the generator', () => {
    const action = {
      payload: {
        username: 'TestUser',
        password: 'TestPassword',
        rememberMe: true
      }
    };
    const generator = login(action);

    it('should call login api', () => {
      expect(generator.next().value).to.deep.equal(call(Api.authentication.login, action.payload));
    });

    it('and then should dispatch LOGIN_SUCCESS action', () => {
      const response = {
        data: {
          id: 1,
          rev: 0,
          role: 'ROLE_ADMIN',
          username: 'admin'
        }
      };
      expect(generator.next(response).value).to.deep.equal(put(loginSuccess(response.data)));
    });
  });

  describe('on login error, the generator', () => {
    const action = {
      payload: {
        username: 'TestUser',
        password: 'TestPassword',
        rememberMe: true
      }
    };
    const generator = login(action);
    generator.next();

    it('should dispatch LOGIN_FAILURE action', () => {
      const error = {
        error: 'error'
      };
      expect(generator.throw(error).value).to.deep.equal(put(loginFailure(error)));
    });
  });

  describe('on logout, the generator', () => {
    const generator = logout();

    it('should call logout api', () => {
      expect(generator.next().value).to.deep.equal(call(Api.authentication.logout));
    });

    it('and then should dispatch LOGOUT_SUCCESS action', () => {
      const response = {
        data: {
          status: 'LOGGED_OUT'
        }
      };

      expect(generator.next(response).value).to.deep.equal(put(logoutSuccess(response.data)));
    });
  });

  describe('on authenticate, the generator', () => {
    const generator = authenticate();

    it('should call authenticate api', () => {
      expect(generator.next().value).to.deep.equal(call(Api.authentication.authenticate));
    });
  });

  describe('on authenticate error, the generator', () => {
    const generator = authenticate();
    generator.next();

    it('should dispatch RESET_AUTHENTICATION_STATE action', () => {
      const error = {
        error: 'error'
      };
      expect(generator.throw(error).value).to.deep.equal(put(resetAuthenticationState(error)));
    });
  });

  describe('on user unauthorized redirect', () => {
    const generator = redirectAndReset();

    it('should dispatch RESET_AUTHENTICATION_STATE action', () => {
      expect(generator.next().value).to.deep.equal(put(resetAuthenticationState()));
    });
  });
});
