import Immutable from 'immutable';
import { handleActions } from 'redux-actions';
import { loginSuccess, logoutSuccess, loginFailure, logoutFailure,
  resetAuthenticationState, resetValidationState } from './actions';

const initialState = {
  isAuthenticated: false,
  role: null,
  token: null,
  loginFailedError: false
};

const authReducer = handleActions({
  [loginSuccess]: (state, action) => state.merge({
    isAuthenticated: true,
    role: action.payload.role,
    token: 'test'
  }),
  [logoutSuccess]: () => Immutable.Map(initialState),
  [loginFailure]: () => Immutable.Map(initialState).merge({
    loginFailedError: true
  }),
  [logoutFailure]: state => state, // What to do in this case?
  [resetAuthenticationState]: () => Immutable.Map(initialState),
  [resetValidationState]: state => state.merge({
    loginFailedError: false
  })
}, Immutable.Map(initialState));

export default authReducer;
