package fi.vincit.aspect.monitoring;

import com.google.common.base.Stopwatch;
import fi.vincit.config.AopConfig;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
@Order(AopConfig.ORDER_PERFORMANCE_MONITORING)
public class RequestLatencyMonitoringAspect implements Ordered {

    private static ConcurrentHashMap<String, MethodStats> methodStats = new ConcurrentHashMap<>();

    private static final boolean LOG_METHOD_PARAMETERS = false;

    /**
     * Periodically log method statistics for every x invocation.
     */
    private static final long STAT_LOG_FREQUENCY = 20;

    /**
     * Generate WARN log message when request processing exceeds time limit
     */
    private static final long METHOD_WARNING_THRESHOLD = 150;

    private int order = Ordered.HIGHEST_PRECEDENCE;

    @Override
    public int getOrder() {
        return order;
    }

    /**
     * Allow overriding of the default order.
     *
     * @param order aspect order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    @Around("execution(public * fi.vincit..*Feature.*(..))")
    public Object logFeature(ProceedingJoinPoint joinPoint) throws Throwable {
        return interceptMethodCall(joinPoint);
    }

    @Around("execution(public * fi.vincit..*ServiceImpl.*(..))")
    public Object logService(ProceedingJoinPoint joinPoint) throws Throwable {
        return interceptMethodCall(joinPoint);
    }

    private static Object interceptMethodCall(ProceedingJoinPoint joinPoint) throws Throwable {
        final Stopwatch stopwatch = Stopwatch.createStarted();

        try {
            return joinPoint.proceed();

        } finally {
            stopwatch.stop();

            final long elapsedTimeMs = stopwatch.elapsed(TimeUnit.MILLISECONDS);

            if (joinPoint.getThis() != null) {
                final Class<?> targetClass = AopUtils.getTargetClass(joinPoint.getThis());
                final Logger logger = LoggerFactory.getLogger(targetClass);

                monitorExecutionTime(logger, joinPoint, elapsedTimeMs);
            }
        }
    }

    private static void monitorExecutionTime(final Logger logger,
                                             final ProceedingJoinPoint joinPoint,
                                             final long elapsedTime) {

        final MethodStats stats = getMethodStatistics(joinPoint);

        if (stats.update(elapsedTime)) {
            printPeriodicStatistics(logger, elapsedTime, stats);

            stats.endPeriod();
        }

        if (elapsedTime > METHOD_WARNING_THRESHOLD) {
            warnAboutExecutionTime(logger, joinPoint, elapsedTime, stats);
        }
    }

    private static void printPeriodicStatistics(Logger logger, long elapsedTime, MethodStats stats) {
        final long avgTime = stats.getTotalTime() / stats.getCount();
        final long runningAvg = (stats.getTotalTime() - stats.getLastTotalTime()) / STAT_LOG_FREQUENCY;

        logger.debug("method: " + stats.getMethodName() + "(), cnt = "
                + stats.getCount() + ", lastTime = "
                + elapsedTime + "ms, avg Time = "
                + avgTime + "ms, runningAvg = "
                + runningAvg + "ms, maxTime = "
                + stats.getMaxTime() + "ms");
    }

    private static MethodStats getMethodStatistics(final ProceedingJoinPoint joinPoint) {
        final String methodName = getMethodName(joinPoint);
        MethodStats stats = methodStats.get(methodName);
        if (stats == null) {
            stats = new MethodStats(methodName);
            methodStats.put(methodName, stats);
        }
        return stats;
    }

    private static String getMethodName(ProceedingJoinPoint joinPoint) {
        final Signature signature = joinPoint.getSignature();
        return signature.getDeclaringTypeName() + "#" + signature.getName();
    }

    private static void warnAboutExecutionTime(final Logger logger, final ProceedingJoinPoint joinPoint,
                                               final long elapsedTime, MethodStats stats) {
        final StringBuilder logMessage = new StringBuilder();
        logMessage.append("Method warning: ");
        logMessage.append(stats.getMethodName());
        if (LOG_METHOD_PARAMETERS) {
            logMethodArguments(joinPoint, logMessage);
        } else {
            logMessage.append("()");
        }
        logMessage.append(", cnt=");
        logMessage.append(stats.getCount());
        logMessage.append(", lastTime = ");
        logMessage.append(elapsedTime);
        logMessage.append("ms");
        logMessage.append(", maxTime = ");
        logMessage.append(stats.getMaxTime());
        logMessage.append("ms");

        logger.warn(logMessage.toString());
    }

    private static void logMethodArguments(ProceedingJoinPoint joinPoint, StringBuilder logMessage) {
        logMessage.append(" with arguments ");
        logMessage.append("(");

        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            logMessage.append(arg).append(",");
        }

        if (args.length > 0) {
            logMessage.deleteCharAt(logMessage.length() - 1);
        }
        logMessage.append(")");
    }

    private static final class MethodStats {
        private final String methodName;
        private long count;
        private long totalTime;
        private long lastTotalTime;
        private long maxTime;

        public MethodStats(final String methodName) {
            this.methodName = methodName;
        }

        synchronized boolean update(final long elapsedTime) {
            this.count = this.count + 1;
            this.totalTime = this.totalTime + elapsedTime;

            if (elapsedTime > this.maxTime) {
                this.maxTime = elapsedTime;
            }

            return this.count % STAT_LOG_FREQUENCY == 0;
        }

        synchronized void endPeriod() {
            //reset the last total time periodically
            this.lastTotalTime = this.totalTime;
        }

        private String getMethodName() {
            return methodName;
        }

        synchronized long getCount() {
            return count;
        }

        synchronized long getTotalTime() {
            return totalTime;
        }

        synchronized long getLastTotalTime() {
            return lastTotalTime;
        }

        synchronized long getMaxTime() {
            return maxTime;
        }
    }
}
