package fi.vincit.security.authentication;

import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserRepository;
import fi.vincit.security.UserInfo;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

public class CustomUserDetailsService implements UserDetailsService {
    @Resource
    private SystemUserRepository userRepository;

    @Override
    @Transactional(readOnly = true, noRollbackFor = RuntimeException.class)
    public UserDetails loadUserByUsername(String username) {
        if (username == null) {
            throw new UsernameNotFoundException("Invalid empty login username");
        }

        try {
            final SystemUser user = userRepository.findByUsername(username);

            if (user != null) {
                return UserInfo.create(user);
            }

        } catch (DataAccessException dae) {
            throw new UsernameNotFoundException("Could not lookup user", dae);
        }

        throw new UsernameNotFoundException("No such username");
    }
}
