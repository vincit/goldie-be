package fi.vincit.security;

import com.google.common.base.Preconditions;
import fi.vincit.feature.account.user.SystemUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

public class UserInfo extends org.springframework.security.core.userdetails.User {
    private static final long serialVersionUID = -1;

    private final Long userId;
    private final SystemUser.Role role;

    public static UserInfo create(final SystemUser user) {
        Objects.requireNonNull(user, "user is null");
        return new UserInfo(user.getUsername(), user.getHashedPassword(), user.isActive(), user.getId(), user.getRole(),
                AuthorityUtils.createAuthorityList(user.getRole().name()));
    }

    public static UserInfo create(final String username, final Long userId, final SystemUser.Role role) {
        return new UserInfo(username, UUID.randomUUID().toString(), true, userId, role,
                AuthorityUtils.createAuthorityList(role.name()));
    }

    public static UserInfo extractFrom(final Authentication authentication) {
        Objects.requireNonNull(authentication, "No authentication available");
        Preconditions.checkState(authentication.isAuthenticated(), "User is not authenticated");
        Objects.requireNonNull(authentication.getPrincipal(), "No principal for authentication");

        if (authentication.getPrincipal() instanceof UserInfo) {
            return UserInfo.class.cast(authentication.getPrincipal());
        }

        throw new IllegalStateException("Authenticate user principal type is unknown: "
                + authentication.getPrincipal().getClass().getSimpleName());
    }

    private UserInfo(final String username, final String password, final boolean isActive,
                     final Long userId, final SystemUser.Role role,
                     final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, isActive, isActive, isActive, isActive, authorities);
        this.role = Objects.requireNonNull(role, "role is null");
        this.userId = Objects.requireNonNull(userId, "userId is null");
    }

    public Authentication createAuthentication() {
        return new PreAuthenticatedAuthenticationToken(this, null, getAuthorities());
    }

    @Nonnull
    public Long getUserId() {
        return userId;
    }

    @Nonnull
    public SystemUser.Role getRole() {
        return role;
    }
}
