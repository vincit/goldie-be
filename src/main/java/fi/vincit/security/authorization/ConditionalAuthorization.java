package fi.vincit.security.authorization;

public interface ConditionalAuthorization {
    boolean applies();
}
