package fi.vincit.security.authorization;

import fi.vincit.feature.common.BaseEntity;
import org.springframework.security.core.Authentication;

import javax.annotation.Nonnull;

public interface EntityAuthorizationStrategy<T extends BaseEntity> {
    Class<T> getEntityClass();

    boolean hasPermission(@Nonnull T target,
                          @Nonnull Enum<?> permission,
                          @Nonnull Authentication authentication);
}
