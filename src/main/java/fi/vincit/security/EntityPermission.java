package fi.vincit.security;

public enum EntityPermission {
    CREATE,
    READ,
    UPDATE,
    DELETE
}
