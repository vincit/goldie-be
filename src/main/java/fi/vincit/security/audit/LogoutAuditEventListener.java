package fi.vincit.security.audit;

import fi.vincit.feature.account.audit.AccountAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LogoutAuditEventListener implements LogoutHandler {
    private static final Logger LOG = LoggerFactory.getLogger(LogoutAuditEventListener.class);

    @Resource
    private AccountAuditService accountAuditService;

    @Override
    @Transactional(noRollbackFor = RuntimeException.class)
    public void logout(HttpServletRequest request,
                       HttpServletResponse response,
                       Authentication authentication) {
        try {
            accountAuditService.auditLogoutEvent(request, authentication);
        } catch (Exception ex) {
            LOG.error("Could not audit logout event", ex);
        }
    }
}
