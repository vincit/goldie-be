package fi.vincit.config.servlet;

import fi.vincit.aspect.web.UncaughtExceptionFilter;
import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

import javax.servlet.ServletContext;
import java.nio.charset.StandardCharsets;

@Order(3)
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
    public static final String DEFAULT_CHARACTER_ENCODING = StandardCharsets.UTF_8.name();

    @Override
    protected void beforeSpringSecurityFilterChain(final ServletContext servletContext) {
        final CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding(DEFAULT_CHARACTER_ENCODING);

        insertFilters(servletContext,
                new UncaughtExceptionFilter(),
                new UrlRewriteFilter(),
                characterEncodingFilter);
    }
}