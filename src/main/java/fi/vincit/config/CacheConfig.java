package fi.vincit.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.cache.Cache;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.caffeine.CaffeineCacheManager;

@Configuration
@EnableCaching
public class CacheConfig extends CachingConfigurerSupport {
    private static final Logger LOG = LoggerFactory.getLogger(CacheConfig.class);

    @Resource
    private Environment env;

    @Override
    public CacheManager cacheManager() {
        return new CaffeineCacheManager() {
            // Expire cache by default in 5 minutes
            private static final int DEFAUT_CACHE_TTL_SECONDS = 60 * 5;
            private static final int DEFAUT_CACHE_SIZE = 1000;

            @Override
            protected Cache createCaffeineCache(final String name) {
                final int ttlSeconds = env.getProperty("cache.ttl." + name, int.class, DEFAUT_CACHE_TTL_SECONDS);
                final int size = env.getProperty("cache.size." + name, int.class, DEFAUT_CACHE_SIZE);

                LOG.info("Creating cache {} with maxSize = {} and ttl = {}", name, size, ttlSeconds);

                return new CaffeineCache(name, Caffeine.newBuilder()
                        .expireAfterWrite(ttlSeconds, TimeUnit.SECONDS)
                        .maximumSize(size)
                        .build());
            }
        };
    }

    @Override
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }
}
