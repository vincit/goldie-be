package fi.vincit.config.hibernate;

import org.hibernate.cfg.ImprovedNamingStrategy;

/**
 * Helper class for reusing the Hibernate ImprovedNamingStrategy classes
 * addUnderscores -method.
 * @author Joonas Haapsaari
 */
class Underscorifier extends ImprovedNamingStrategy {
    /**
     * Converts camel case to underscore-delimited format. Converts '.'s to underscores.
     * @param name the name
     * @return the converted name
     */
    public static String addUnderscores(String name) {
        return ImprovedNamingStrategy.addUnderscores(name);
    }
}
