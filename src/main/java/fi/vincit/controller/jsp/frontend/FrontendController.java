package fi.vincit.controller.jsp.frontend;

import net.rossillo.spring.web.mvc.CacheControl;
import net.rossillo.spring.web.mvc.CachePolicy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FrontendController {
    public static final String JSP_CLIENT_LOADER = "frontend/client";

    /*
     * Loader for SPA-frontend
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @CacheControl(policy = {CachePolicy.PUBLIC}, maxAge = 600)
    public String showClient(@SuppressWarnings("unused") Model model) {
        return JSP_CLIENT_LOADER;
    }
}
