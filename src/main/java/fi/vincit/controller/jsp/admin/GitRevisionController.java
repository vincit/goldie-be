package fi.vincit.controller.jsp.admin;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * GoldieApplicationContext reads properties from target/classes/git.properties and
 * makes them available via Environment.
 *
 * git.properties is copied to target/classes from src/main/resources in resources-phase.
 * Then it's overwritten by git-commit-id-plugin if plugin's phase is executed.
 */
@Controller
@RequestMapping("/api/revision")
public class GitRevisionController {

    @JsonInclude(JsonInclude.Include.ALWAYS)
    public static class GitRepositoryState {
        public String branch;                  // =${git.branch}
        public String describe;                // =${git.commit.id.describe}
        public String commitId;                // =${git.commit.id}
        public String buildUserName;           // =${git.build.user.name}
        public String buildUserEmail;          // =${git.build.user.email}
        public String buildTime;               // =${git.build.time}
        public String commitUserName;          // =${git.commit.user.name}
        public String commitUserEmail;         // =${git.commit.user.email}
        public String commitMessageFull;       // =${git.commit.message.full}
        public String commitMessageShort;      // =${git.commit.message.short}
        public String commitTime;              // =${git.commit.time}

        public GitRepositoryState() {
        }

        public GitRepositoryState(Environment env) {
            this.branch = env.getProperty("git.branch");
            this.describe = env.getProperty("git.commit.id.describe");
            this.commitId = env.getProperty("git.commit.id");
            this.buildUserName = env.getProperty("git.build.user.name");
            this.buildUserEmail = env.getProperty("git.build.user.email");
            this.buildTime = env.getProperty("git.build.time");
            this.commitUserName = env.getProperty("git.commit.user.name");
            this.commitUserEmail = env.getProperty("git.commit.user.email");
            this.commitMessageShort = env.getProperty("git.commit.message.short");
            this.commitMessageFull = env.getProperty("git.commit.message.full");
            this.commitTime = env.getProperty("git.commit.time");
        }
    }

    @Resource
    private Environment env;

    @RequestMapping
    @ResponseBody
    public GitRepositoryState checkGitRevision() {
        return new GitRepositoryState(env);
    }
}
