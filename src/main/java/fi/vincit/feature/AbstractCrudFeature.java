package fi.vincit.feature;

import com.google.common.base.Preconditions;
import fi.vincit.feature.common.exception.NotFoundException;
import fi.vincit.feature.account.ActiveUserService;
import fi.vincit.feature.common.BaseEntity;
import fi.vincit.feature.common.BaseEntityDTO;
import fi.vincit.security.EntityPermission;
import fi.vincit.util.DtoUtil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * @param <ID> Entity's id class, often Long.class
 * @param <E>  Entity's class
 * @param <D>  DTO class corresponding to the entity
 */
public abstract class AbstractCrudFeature<ID extends Serializable,
        E extends BaseEntity<ID>,
        D extends BaseEntityDTO<ID>> {

    @Resource
    protected ActiveUserService activeUserService;

    private final Class<? extends E> entityClass;

    @SuppressWarnings({"unchecked"})
    protected AbstractCrudFeature() {
        this.entityClass =
                (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    protected abstract JpaRepository<E, ID> getRepository();

    protected abstract void updateEntity(E entity, D dto);

    protected abstract D toDTO(E entity);

    @Transactional(readOnly = true)
    public D read(ID id) {
        return toDTO(requireEntity(id, EntityPermission.READ));
    }

    @Transactional
    public D create(D dto) {
        final E entity = createEntity();

        updateEntity(entity, dto);

        // Permission check
        activeUserService.assertHasPermission(entity, EntityPermission.CREATE);

        return toDTO(getRepository().saveAndFlush(entity));
    }

    private E createEntity() {
        try {
            return entityClass.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException("Could not create entity", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Could access constructor", e);
        }
    }

    @Transactional
    public D update(D dto) {
        final E entity = requireEntity(dto.getId(), EntityPermission.UPDATE);

        DtoUtil.assertNoVersionConflict(entity, dto);

        updateEntity(entity, dto);

        // Must use saveAndFlush() to update returned consistencyVersion == dto.revision
        return toDTO(getRepository().saveAndFlush(entity));
    }

    @Transactional
    public void delete(ID id) {
        delete(requireEntity(id, EntityPermission.DELETE));
    }

    protected void delete(E entity) {
        getRepository().delete(entity);
    }

    private E requireEntity(final ID id, final EntityPermission permission) {
        Preconditions.checkNotNull(id, "Entity primary key is required");

        final E entity = getRepository().findOne(id);

        if (entity == null) {
            throw new NotFoundException("No such " + entityClass.getCanonicalName() + " id=" + id);
        }

        // Permission check
        activeUserService.assertHasPermission(entity, permission);

        return entity;
    }
}
