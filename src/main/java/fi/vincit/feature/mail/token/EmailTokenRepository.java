package fi.vincit.feature.mail.token;

import fi.vincit.feature.account.user.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmailTokenRepository extends JpaRepository<EmailToken, String> {
    @Query("SELECT t FROM EmailToken t WHERE t.user = ?1 AND t.revokedAt IS NULL")
    List<EmailToken> findNonRevokedByUser(SystemUser user);

    @Query("SELECT t FROM EmailToken t WHERE t.email LIKE ?1 AND t.revokedAt IS NULL")
    List<EmailToken> findNonRevokedByEmail(String email);
}