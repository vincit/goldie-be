package fi.vincit.feature.seo;

import fi.vincit.feature.seo.dto.SiteMapUrlSet;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Interface for sitemap generator services. All of the services implementing this
 * interface will be used to generate the sitemaps. The name of the sitemap urlset
 * is the name of the service, so you might want to name the services wisely.
 *
 * You may use the AbstractSitemapGeneratorService as the base class
 * when implementing the sitemap urlset generating service.
 *
 * Usage example:
 * <code>
 * @Service("sitemap_stories")
 * public class StoriesSitemapGeneratorService extends AbstractSitemapGeneratorService {
 *      @Resource
 *      private StoryService storyService;
 *
 *      @Override
 *      public SiteMapUrlSet generateSitemap() {
 *          SiteMapUrlSet urlSet = new SiteMapUrlSet();
 *          List<Long> storyIds = storyService.getStoryIds();
 *
 *          for (Long storyId : storyIds) {
 *              SiteMapUrl url = new SiteMapUrl.Builder()
 *                  .withLoc(getServerUrl() + "/story/" + storyId)
 *                  .withChangefreq(SiteMapUrl.ChangeFrequency.DAILY)
 *                  .build();
 *
 *              urlSet.addUrl(url);
 *          }
 *
 *          return urlSet;
 *      }
 * }
 *
 * </code>
 */
public interface SitemapGeneratorService {
    /**
     * Generates a sitemap urlset document.
     * @return the sitemap urlset document
     * @param servletUriComponentsBuilder
     */
    SiteMapUrlSet generateSitemap(ServletUriComponentsBuilder uriBuilder);

    /**
     * Returns the last modified value for the sitemap urlset document. The
     * value is used in the siteindex document if available.
     *
     * Null-value means that the lastmod value is not defined.
     * @return the last modified ISO date time value
     */
    String lastModified();
}
