package fi.vincit.feature.seo.dto;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * XML element for sitemap sitemap
 * @see <a href="http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd</a>
 */
@XmlAccessorType(value = XmlAccessType.NONE)
@XmlRootElement(name = "sitemap")
public class SiteMapEntry {
    @XmlElement
    private String loc;

    @XmlElement
    private String lastmod;

    public SiteMapEntry() {}

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getLastmod() {
        return lastmod;
    }

    public void setLastmod(String lastmod) {
        this.lastmod = lastmod;
    }

    public void setLastmod(DateTime lastmod) {
        this.lastmod = lastmod.toString();
    }
}
