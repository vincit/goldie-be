package fi.vincit.feature.seo.dto;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * XML element for sitemap sitemapindex
 * @see <a href="http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd</a>
 */
@XmlAccessorType(value = XmlAccessType.NONE)
@XmlRootElement(name = "sitemapindex")
public class SiteMapIndex {
    @XmlElements({@XmlElement(name = "sitemap", type = SiteMapEntry.class )})
    private List<SiteMapEntry> sitemaps = new ArrayList<>();

    public SiteMapIndex() {}

    public List<SiteMapEntry> getSitemaps() {
        return sitemaps;
    }

    public void setSitemaps(List<SiteMapEntry> sitemaps) {
        this.sitemaps = sitemaps;
    }

    public void addSitemap(SiteMapEntry siteMap) {
        sitemaps.add(siteMap);
    }
}
