package fi.vincit.feature.seo;

import fi.vincit.feature.seo.dto.SiteMapUrl;
import fi.vincit.feature.seo.dto.SiteMapUrlSet;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Sitemap generator for "static" SPA pages.
 *
 * This service lists static SPA pages for search indexing. Typical static pages are:
 * - Main page
 * - Search page
 * - Register -page
 * - Info pages
 *
 * Add separate sitemap generator services for dynamic pages such as listing of
 * products, end-user messages, etc. Please make sure that you use a reasonable
 * name for the service, as it is used in the sitemap index file to identify
 * the sitemap for the service.
 *
 * Each sitemap generator service must implement the {@link SitemapGeneratorService} interface
 * and also be a Spring service.
 */
@Service("static_sitemap")
public class StaticSitemapGeneratorService implements SitemapGeneratorService {

    @Override
    public SiteMapUrlSet generateSitemap(final ServletUriComponentsBuilder uriBuilder) {
        SiteMapUrlSet urlSet = new SiteMapUrlSet();
        SiteMapUrl url = new SiteMapUrl.Builder()
                .withLoc(uriBuilder.replacePath("/").toUriString())
                .withChangefreq(SiteMapUrl.ChangeFrequency.DAILY)
                .build();

        urlSet.addUrl(url);

        return urlSet;
    }

    /**
     * returns null as the last modified value,
     * which means that it is not defined for the sitemap. You may override the
     * value as you wish
     */
    @Override
    public String lastModified() {
        return null;
    }
}
