package fi.vincit.feature.storage;

import fi.vincit.feature.storage.metadata.FileType;
import fi.vincit.feature.storage.metadata.PersistentFileMetadata;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

public interface FileStorageService {
    boolean exists(UUID uuid);

    /**
     * Get file metadata
     *
     * @param uuid file unique id
     * @return file metadata entity
     * @throws java.lang.IllegalArgumentException if no metadata with given UUID is found.
     */
    PersistentFileMetadata getMetadata(UUID uuid);

    /**
     * Store file content and metadata using various storage implementations.
     *
     * @param fileType    General purpose of the file is used to control storage options
     * @return File metadata entity suitable as reference from domain classes such as Thumbnail, Upload, Report etc.
     * @throws IOException
     */
    PersistentFileMetadata storeFile(UUID uuid,
                                     byte[] content,
                                     FileType fileType,
                                     String contentType,
                                     String originalFilename) throws IOException;

    PersistentFileMetadata storeFile(UUID uuid,
                                     MultipartFile multipartFile,
                                     FileType fileType) throws IOException;

    /**
     * Get file data
     *
     * @param uuid file unique id
     * @return raw file data as byte array
     * @throws IOException
     * @throws java.lang.IllegalArgumentException if no metadata with given UUID is found.
     * @throws java.lang.IllegalStateException    if content has been deleted
     */
    byte[] getBytes(UUID uuid) throws IOException;

    /**
     * Remove file data from storage.
     *
     * @param uuid file unique id
     * @throws java.lang.IllegalArgumentException if no metadata with given UUID is found.
     */
    void remove(UUID uuid);
}
