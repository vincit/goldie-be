package fi.vincit.feature.storage.metadata;

// Storage method
public enum StorageType {
    LOCAL_DATABASE,
    LOCAL_FOLDER
}
