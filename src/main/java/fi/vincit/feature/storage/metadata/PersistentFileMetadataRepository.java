package fi.vincit.feature.storage.metadata;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PersistentFileMetadataRepository extends JpaRepository<PersistentFileMetadata, UUID> {
}
