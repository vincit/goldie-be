package fi.vincit.feature.logmonitor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import static java.util.stream.Collectors.toList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Stores logger names and counts by hour and provides methods for accessing alarming logger names.
 *
 * Intended to detect anomalies which manifest as many log rows.
 *
 * Define <i>mail.address.to.admin</i> in config.properties.
 *
 * Note that writing to log from this class might lead to unexpected behaviour.
 */
public class LogMonitor {

    private static final Logger LOG = LoggerFactory.getLogger(LogMonitor.class);
    private static final TreeMap<String, Map<String, Integer>> COUNT_BY_LOGGER_NAME_BY_TIME = new TreeMap<>();
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH");

    // Hours (see DATE_FORMAT). Must be >= 1 and should be <= 24. To include daily batch jobs
    // this should be 24, but then it takes 24 hours for this feature to kick in.
    static final int HISTORY_WINDOW_SIZE = 24;
    // How many positions a logger name must have climbed to trigger an alarm.
    private static final int POSITION_CHANGE_THRESHOLD = 15;
    // How many of the top loggers are checked.
    static final int LOGGER_NAMES_TO_CHECK = 10;
    // Unless logging count per hour is equal to this or more, logger is ignored. This is needed for
    // (test) environments where there is very little logging at times.
    static final int MIN_LOGGING_COUNT = 40;
    
    public static void addLoggingEvent(long timestamp, String loggerName) {
        increaseCount(getTime(timestamp), loggerName);
    }

    private static String getTime(long timestamp) {
        synchronized (DATE_FORMAT) {
            return DATE_FORMAT.format(new Date(timestamp));
        }
    }

    private static void increaseCount(String time, String loggerName) {
        synchronized (COUNT_BY_LOGGER_NAME_BY_TIME) {
            Map<String, Integer> countByLoggerName = COUNT_BY_LOGGER_NAME_BY_TIME.get(time);
            if (countByLoggerName == null) {
                countByLoggerName = new HashMap<>();
                COUNT_BY_LOGGER_NAME_BY_TIME.put(time, countByLoggerName);
            }

            countByLoggerName.put(loggerName, countByLoggerName.getOrDefault(loggerName, 0) + 1);
        }
    }

    // To save memory.
    public static void removeUnusedOldData() {
        synchronized (COUNT_BY_LOGGER_NAME_BY_TIME) {
            // +2 latest entries (another one might be unfinished)
            int entriesToRemove = COUNT_BY_LOGGER_NAME_BY_TIME.size() - (HISTORY_WINDOW_SIZE + 2);

            Iterator<String> iterator = COUNT_BY_LOGGER_NAME_BY_TIME.keySet().iterator();
            while (entriesToRemove-- > 0) {
                String key = iterator.next();
                LOG.trace("Removing {}", key);
                iterator.remove();
            }
        }
    }

    public static List<LogMonitorResultData> getAlarmingLoggerNamesAndCounts() {
        List<Map<String, Integer>> entries;
        synchronized (COUNT_BY_LOGGER_NAME_BY_TIME) {
            // Don't write to log inside synchronization to prevent deadlocks
            // (via AppenderBase.doAppend() -> LogMonitorAppender.append() -> LogMonitor.addLoggingEvent())!
            entries = getEntriesLatestFirst();
        }
        if (LOG.isTraceEnabled()) {
            LOG.trace(COUNT_BY_LOGGER_NAME_BY_TIME.toString());
        }

        int availableHistoryEntries = entries.size() - 1;
        if (availableHistoryEntries < HISTORY_WINDOW_SIZE) {
            LOG.debug("Not enough history entries: {} (required {})", availableHistoryEntries, HISTORY_WINDOW_SIZE);
            return Collections.emptyList();
        }

        Map<String, Integer> previousLoggers = combine(entries.subList(1, HISTORY_WINDOW_SIZE + 1));
        Map<String, Integer> latestLoggers = removeSmallLoggers(entries.get(0));

        return getAlarmingLoggerNamesAndCounts(previousLoggers, latestLoggers);
    }

    private static List<Map<String, Integer>> getEntriesLatestFirst() {
        List<Map<String, Integer>> entries = new ArrayList<>(COUNT_BY_LOGGER_NAME_BY_TIME.size());
        COUNT_BY_LOGGER_NAME_BY_TIME.forEach((time, entry) -> entries.add(entry));
        Collections.reverse(entries);

        // Assume something has been written to log before this method is called.
        String latestTime = COUNT_BY_LOGGER_NAME_BY_TIME.descendingKeySet().iterator().next();
        String currentTime = getTime(System.currentTimeMillis());
        // This is currently executed at the beginning of each hour. Check if some logger has
        // already written to log with the same time key.
        boolean latestEntryIsUnfinished = currentTime.equals(latestTime);
        if (latestEntryIsUnfinished) {
            entries.remove(0);
        }
        return entries;
    }

    private static List<LogMonitorResultData> getAlarmingLoggerNamesAndCounts(
            Map<String, Integer> previousLoggers,
            Map<String, Integer> latestLoggers) {

        List<String> previousLoggerNames = getLoggerNamesSortedByCount(previousLoggers);
        List<String> latestLoggerNames = getLoggerNamesSortedByCount(latestLoggers);
        List<LogMonitorResultData> result = new ArrayList<>();
        for (int pos = 0; pos < Math.min(LOGGER_NAMES_TO_CHECK, latestLoggerNames.size()); pos++) {
            String loggerName = latestLoggerNames.get(pos);
            int previousPosition = previousLoggerNames.indexOf(loggerName);
            if (previousPosition == -1 || (previousPosition - pos) >= POSITION_CHANGE_THRESHOLD) {
                result.add(new LogMonitorResultData(
                        loggerName, latestLoggers.get(loggerName), pos, previousPosition - pos));
            }
        }
        
        // Processing is finished (even though we are still inside synchronized-block),
        // so we can write to log.
        if (!result.isEmpty() || LOG.isTraceEnabled()) {
            // Not trace on purpose.
            LOG.debug("Most used latest loggers are: {}",
                    getFirstNames(latestLoggerNames, LOGGER_NAMES_TO_CHECK));
            LOG.debug("Most used previous loggers (of {}) are: {}",
                    previousLoggerNames.size(),
                    getFirstNames(previousLoggerNames, LOGGER_NAMES_TO_CHECK * 4));
        }

        LOG.debug("Alarming loggers: {}", result);
        return result;
    }

    private static String getFirstNames(List<String> list, int count) {
        return String.join(", ", list.subList(0, Math.min(list.size(), count)));
    }

    static Map<String, Integer> combine(List<Map<String, Integer>> historyEntries) {
        Map<String, Integer> result = new HashMap<>();
        for (Map<String, Integer> historyEntry : historyEntries) {
            historyEntry.forEach((loggerName, count) -> {
                result.put(loggerName, result.getOrDefault(loggerName, 0) + count);
            });
        }
        return result;
    }

    // At nighttime when there are very few users it might be too easy to get to the top even with
    // small logging amounts. Remove those here.
    private static Map<String, Integer> removeSmallLoggers(Map<String, Integer> entries) {
        Map<String, Integer> result = new HashMap<>();
        entries.forEach((name, count) -> {
            if (count >= MIN_LOGGING_COUNT) {
                result.put(name, count);
            }
        });
        return result;
    }

    /**
     *
     * @param countsByNames
     * @return List of logger names. Logger with highest count is first.
     */
    static List<String> getLoggerNamesSortedByCount(Map<String, Integer> countsByNames) {
        List<LoggerNameAndCount> loggerNamesAndCounts = new ArrayList<>();
        countsByNames.forEach((loggerName, count) ->
                loggerNamesAndCounts.add(new LoggerNameAndCount(loggerName, count)));
        loggerNamesAndCounts.sort((a, b) -> b.count.compareTo(a.count));
        return loggerNamesAndCounts.stream().map(o -> o.loggerName).collect(toList());
    }

    // Helper class for sorting by count.
    private static class LoggerNameAndCount {
        private final String loggerName;
        private final Integer count;

        public LoggerNameAndCount(String loggerName, Integer count) {
            this.loggerName = loggerName;
            this.count = count;
        }
    }
}
