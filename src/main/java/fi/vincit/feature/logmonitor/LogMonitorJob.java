package fi.vincit.feature.logmonitor;

import com.github.jknack.handlebars.Handlebars;
import fi.vincit.config.quartz.QuartzScheduledJob;
import fi.vincit.feature.RuntimeEnvironmentUtil;
import fi.vincit.feature.mail.MailMessageDTO;
import fi.vincit.feature.mail.MailService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

@QuartzScheduledJob(name = "LogMonitorJob", cronExpression = "0 0 * * * ?")
public class LogMonitorJob implements Job {

    private static final Logger LOG = LoggerFactory.getLogger(LogMonitorJob.class);

    @Value("${mail.address.to.admin}")
    private String mailAddressToAdmin;
    @Resource
    private MailService mailService;
    @Resource
    private Handlebars handlebars;
    @Resource
    private RuntimeEnvironmentUtil runtimeEnvironmentUtil;

    // Uncomment this if not using Quarz: @Scheduled(cron = "0 0 * * * ?")
    @Override
    public void execute(final JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<LogMonitorResultData> results = LogMonitor.getAlarmingLoggerNamesAndCounts();

        sendWarningEmail(results);
        
        LogMonitor.removeUnusedOldData();
    }

    private void sendWarningEmail(final List<LogMonitorResultData> results) {
        if (results.isEmpty()) {
            return;
        }
        if (StringUtils.isEmpty(mailAddressToAdmin)) {
            LOG.error("Property 'mail.address.to.admin' not set in config.properties!");
            return;
        }
        
        final Map<String, Object> params = new HashMap<>();
        params.put("environmentIdentifier", getEnvironmentIdentifier());

        final List<String> list = new ArrayList<>();
        results.forEach(result -> {
            list.add(String.format("%d. (%s) %s (%d kertaa)",
                    result.position,
                    result.positionChange != null ? ("+" + result.positionChange) : "uusi",
                    result.name,
                    result.count));
        });
        params.put("list", list);

        MailMessageDTO.Builder builder = new MailMessageDTO.Builder()
                .withTo(mailAddressToAdmin)
                .withSubject("Tarkista palvelimen loki")
                .withHandlebarsBody(handlebars, "logger_names", params);
        mailService.sendImmediate(builder);
    }

    private String getEnvironmentIdentifier() {
        // We could use "InetAddress.getLocalHost().getHostName()", but it might look strange.
        // Environment id and email sender together identify the source environment.
        return runtimeEnvironmentUtil.getEnvironmentId();
    }
}
