package fi.vincit.feature.logmonitor;

public class LogMonitorResultData {

    public String name;
    public int count;
    public int position;
    // Null if logger is new
    public Integer positionChange;

    public LogMonitorResultData(String name, int count, int positionZeroBased, int positionChange) {
        this.name = name;
        this.count = count;
        this.position = positionZeroBased + 1;
        this.positionChange = positionChange >= 0 ? positionChange : null;
    }

    @Override
    public String toString() {
        return "LogResultData{" + "name=" + name + ", count=" + count + ", position=" + position
                + ", positionChange=" + positionChange + '}';
    }
}
