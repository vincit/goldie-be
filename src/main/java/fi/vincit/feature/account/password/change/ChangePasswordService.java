package fi.vincit.feature.account.password.change;

import com.google.common.base.Preconditions;
import fi.vincit.feature.account.user.SystemUser;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.inject.Inject;

@Service
@Transactional(propagation = Propagation.MANDATORY)
public class ChangePasswordService {
    private PasswordEncoder passwordEncoder;

    @Inject
    public ChangePasswordService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Confirm user current password matched given value.
     *
     * @param user          target system user
     * @param plainPassword password in plain-text
     * @return true, if given password is correct
     */
    @Transactional(readOnly = true, noRollbackFor = RuntimeException.class)
    public boolean confirmPassword(final SystemUser user, final String plainPassword) {
        Preconditions.checkNotNull(plainPassword, "No password to confirm given");
        return user.getHashedPassword() != null && passwordEncoder.matches(plainPassword, user.getHashedPassword());
    }

    /**
     * Attempt to change user password and apply password policy.
     *
     * @param user          target system user
     * @param plainPassword password in plain-text
     */
    @Transactional(noRollbackFor = RuntimeException.class)
    public void setUserPassword(final SystemUser user, final String plainPassword) {
        Preconditions.checkArgument(StringUtils.hasText(plainPassword), "Empty password");
        user.setPasswordAsPlaintext(plainPassword, passwordEncoder);
    }
}
