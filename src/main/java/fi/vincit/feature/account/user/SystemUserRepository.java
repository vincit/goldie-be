package fi.vincit.feature.account.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SystemUserRepository extends JpaRepository<SystemUser, Long>, QueryDslPredicateExecutor<SystemUser> {
    SystemUser findByUsername(String username);

    List<SystemUser> findByEmail(String email);

    // Native query bypasses @Where annotation on SystemUser
    @Query(value = "SELECT CASE WHEN COUNT(u.user_id) > 0 THEN TRUE ELSE FALSE END"
            + " FROM system_user u WHERE u.username = :username AND u.user_id <> :userId", nativeQuery = true)
    boolean isUsernameTaken(@Param("username") String username, @Param("userId") Long userId);

    @Query(value = "SELECT CASE WHEN COUNT(u.user_id) > 0 THEN TRUE ELSE FALSE END"
            + " FROM system_user u WHERE u.username = :username", nativeQuery = true)
    boolean isUsernameTaken(@Param("username") String username);
}
