package fi.vincit.feature.account.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
public class SystemUserListFeature {

    @Resource
    private SystemUserRepository userRepository;

    @Resource
    private SystemUserDTOTransformer dtoTransformer;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional(readOnly = true)
    public Page<SystemUserDTO> list(final Pageable pageRequest) {
        return dtoTransformer.apply(userRepository.findAll(pageRequest), pageRequest);
    }
}
