package fi.vincit.feature.account.user;

import fi.vincit.security.authorization.AbstractEntityAuthorization;
import org.springframework.stereotype.Component;


import static fi.vincit.feature.account.user.SystemUser.Role.ROLE_ADMIN;

@Component
public class SystemUserAuthorization extends AbstractEntityAuthorization<SystemUser> {
    public SystemUserAuthorization() {
        allowCRUD(ROLE_ADMIN);
    }
}
