package fi.vincit.feature.account.user;

import fi.vincit.feature.AbstractCrudFeature;
import fi.vincit.feature.account.password.change.ChangePasswordService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class SystemUserCrudFeature extends AbstractCrudFeature<Long, SystemUser, SystemUserDTO> {

    @Resource
    private SystemUserRepository userRepository;

    @Resource
    private ChangePasswordService changePasswordService;

    @Resource
    private SystemUserDTOTransformer dtoTransformer;

    @Override
    protected JpaRepository<SystemUser, Long> getRepository() {
        return userRepository;
    }

    @Override
    protected SystemUserDTO toDTO(final SystemUser entity) {
        return dtoTransformer.apply(entity);
    }

    @Override
    protected void updateEntity(SystemUser user, SystemUserDTO userDTO) {
        activeUserService.getActiveUserInfo().ifPresent(activeUserInfo -> {
            if (activeUserInfo.getRole().isAdmin()) {
                SystemUserDTOTransformer.toEntity(user, userDTO);

                if (userDTO.getPassword() != null) {
                    changePasswordService.setUserPassword(user, userDTO.getPassword());
                }
            }
        });
    }

    @Override
    protected void delete(SystemUser user) {
        user.setActive(false);
        user.softDelete();
    }
}
