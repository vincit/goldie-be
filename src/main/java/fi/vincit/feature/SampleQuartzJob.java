package fi.vincit.feature;

import fi.vincit.config.quartz.QuartzScheduledJob;
import fi.vincit.feature.account.user.SystemUser;
import fi.vincit.feature.account.user.SystemUserRepository;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.List;

import static java.util.stream.Collectors.toList;

@DisallowConcurrentExecution
@QuartzScheduledJob(name = "SampleJob", cronExpression = "0 * * * * ?")
public class SampleQuartzJob implements Job {
    private static final Logger LOG = LoggerFactory.getLogger(SampleQuartzJob.class);

    @Resource
    private SystemUserRepository userRepository;

    @Override
    public void execute(final JobExecutionContext jobExecutionContext) throws JobExecutionException {
        final List<String> list = userRepository.findAll().stream()
                .map(SystemUser::getUsername)
                .collect(toList());

        LOG.info("Result: {}", list);
    }
}
