/*
 * Copyright 2012-2016 Jarkko Kaura
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fi.vincit.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.Optional;

public final class ClassUtils {

    private ClassUtils() {
        throw new AssertionError();
    }

    @Nullable
    public static Class<?> resolveGenericType(@Nonnull final Field field) {
        Objects.requireNonNull(field);
        final Type[] actualTypeArgs = ((ParameterizedType) field.getGenericType()).getActualTypeArguments();
        return actualTypeArgs.length == 1 ? (Class<?>) actualTypeArgs[0] : null;
    }

    @Nonnull
    public static Class<?> getTypeArgumentOfSuperClass(
            @Nonnull final Object object, final Class<?> superClass, final int typeArgumentIndex) {

        Objects.requireNonNull(object, "object must not be null");
        Objects.requireNonNull(superClass, "superClass must not be null");
        Preconditions.checkArgument(typeArgumentIndex >= 0, "typeArgumentIndex must not be negative");

        final Optional<ParameterizedType> paramType =
                findParameterizedParentTypeOfClass(object.getClass(), superClass);

        if (!paramType.isPresent()) {
            throw new IllegalArgumentException(String.format(
                    "Did not find parameterized interface of class %s for object of class %s",
                    superClass.getName(),
                    object.getClass().getName()));
        }

        final Type[] typeArguments = paramType.get().getActualTypeArguments();

        if (typeArguments.length == 0) {
            throw new IllegalArgumentException(
                    "The interface does not have type arguments: " + superClass.getName());
        }

        return (Class<?>) typeArguments[typeArgumentIndex];
    }

    @Nonnull
    public static Optional<ParameterizedType> findParameterizedParentTypeOfClass(
            @Nonnull final Class<?> clazz, @Nonnull final Class<?> expectedType) {

        Objects.requireNonNull(clazz, "clazz must not be null");
        Objects.requireNonNull(expectedType, "expectedType must not be null");

        final Type genericSuperclass = clazz.getGenericSuperclass();
        final Type[] genericInterfaces = clazz.getGenericInterfaces();

        for (final Type type : Lists.asList(genericSuperclass, genericInterfaces)) {
            if (type != null) {
                if (type instanceof ParameterizedType) {
                    final ParameterizedType parameterizedType = (ParameterizedType) type;
                    final Type rawType = parameterizedType.getRawType();

                    if (rawType instanceof Class) {
                        final Class<?> rawClass = (Class<?>) rawType;

                        if (expectedType.equals(rawClass)) {
                            return Optional.of(parameterizedType);
                        }

                        final Optional<ParameterizedType> match =
                                findParameterizedParentTypeOfClass(rawClass, expectedType);
                        if (match.isPresent()) {
                            return match;
                        }
                    }
                } else if (type instanceof Class) {
                    final Optional<ParameterizedType> match =
                            findParameterizedParentTypeOfClass((Class<?>) type, expectedType);
                    if (match.isPresent()) {
                        return match;
                    }
                }
            }
        }

        return Optional.empty();
    }

    @Nonnull
    public static <T> Optional<T> cast(@Nullable final Object obj, @Nonnull final Class<T> refClass) {
        Objects.requireNonNull(refClass, "refClass must not be null");

        return Optional.ofNullable(obj != null && refClass.isAssignableFrom(obj.getClass()) ? refClass.cast(obj) : null);
    }

}