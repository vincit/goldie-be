package fi.vincit.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public final class JCEUtil {
    private static final Logger LOG = LoggerFactory.getLogger(JCEUtil.class);

    public static void removeJavaCryptographyAPIRestrictions() {
        try {
            Field field = Class.forName("javax.crypto.JceSecurity").
                    getDeclaredField("isRestricted");
            field.setAccessible(true);

            // Since Java 8u102 the "isRestricted" field is marked as final. Undo that.
            // See http://stackoverflow.com/questions/3301635/change-private-static-final-field-using-java-reflection
            // (because the "isRestricted" field is not a constant this should work).
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            field.set(null, Boolean.FALSE);
        } catch (Exception ex) {
            LOG.error("Could not remove JCE restrictions", ex);
        }
    }

    private JCEUtil() {
        throw new AssertionError();
    }
}
