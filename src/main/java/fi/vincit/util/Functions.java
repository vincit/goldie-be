package fi.vincit.util;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintViolation;
import java.util.function.Function;

public class Functions {
    public static final Function<ConstraintViolation<?>, String> CONSTRAINT_VIOLATION_TO_STRING = violation -> {
        if (violation == null) {
            return null;
        }

        final String violationMessage = violation.getMessage() != null && !violation.getMessage().matches("^\\{.+\\}$")
                ? String.format(" %s, was", violation.getMessage())
                : "";

        return String.format(
                "%s.%s%s: %s [@%s]",
                violation.getRootBeanClass().getSimpleName(),
                violation.getPropertyPath(),
                violationMessage,
                StringUtils.quoteIfString(violation.getInvalidValue()),
                violation.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName());
    };

    private Functions() {
        throw new AssertionError();
    }
}
