package fi.vincit.validation;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Locale;
import java.util.Set;

public class ISO3166CountryCodeValidator implements ConstraintValidator<ISO3166CountryCode, String> {
    private static final Set<String> VALID_COUNTRY_CODES;

    static {
        VALID_COUNTRY_CODES = Sets.newHashSet(Arrays.asList(Locale.getISOCountries()));
        VALID_COUNTRY_CODES.add("ZZ");
    }

    public static boolean isValidCountryCode(String countryCode) {
        return VALID_COUNTRY_CODES.contains(countryCode);
    }

    @Override
    public void initialize(ISO3166CountryCode iso3166CountryCode) {
    }

    @Override
    public boolean isValid(final String countryCode, final ConstraintValidatorContext constraintValidatorContext) {
        return Strings.isNullOrEmpty(countryCode) || isValidCountryCode(countryCode);
    }

    /**
     * This is the code used to generate the enum content
     */
    public static void main(String[] args) {
        final String[] codes = java.util.Locale.getISOCountries();

        for (final String isoCode: codes) {
            final Locale locale = new Locale("", isoCode);
            System.out.println("'" + isoCode + "': '" + locale.getDisplayCountry(Locale.ENGLISH) + "',");
            //System.out.println("'" + isoCode + "': '" + locale.getDisplayCountry(locale) + "',");
        }
    }
}
