package fi.vincit.validation;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class FinnishBusinessIdValidator implements ConstraintValidator<FinnishBusinessId, String> {

    public static final Pattern REGEX_PATTERN = Pattern.compile("[0-9]{7}-[0-9]");
    public static final int[] WEIGHTS = new int[]{7, 9, 10, 5, 8, 4, 2};
    public static final int VALID_LENGTH = 7 + 2;

    private boolean verifyChecksum;

    @Override
    public void initialize(FinnishBusinessId finnishBusinessId) {
        this.verifyChecksum = finnishBusinessId.verifyChecksum();
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext) {
        return validate(value, this.verifyChecksum);
    }

    public static boolean validate(final String value, final boolean verifyChecksum) {
        if (!StringUtils.hasText(value)) {
            return true;
        }

        if (value.length() != VALID_LENGTH) {
            return false;
        }

        if (!REGEX_PATTERN.matcher(value).matches()) {
            return false;
        }

        if (!verifyChecksum) {
            return true;
        }

        final char checksum = value.charAt(8);
        final char calculatedChecksum = calculateChecksum(value);

        return checksum == calculatedChecksum;
    }

    public static char calculateChecksum(final String s) {
        int sum = 0;

        for (int i = 0; i < WEIGHTS.length; i++) {
            sum += Character.getNumericValue(s.charAt(i)) * WEIGHTS[i];
        }

        final int remainder = sum % 11;

        if (remainder == 0) {
            return '0';
        } else if (remainder == 1) {
            return 'x';
        } else {
            return Character.forDigit(11 - remainder, 10);
        }
    }
}
