/*
 * Copyright 2012-2014 Jarkko Kaura
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fi.vincit.test;

import fi.vincit.util.Functions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Persistable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import java.util.Objects;

import static java.util.stream.Collectors.joining;

/**
 * A Spring bean to be used for persisting one or multiple entities in a single
 * Spring-managed transaction.
 *
 * @author Jarkko Kaura
 */
@Component
public class EntityPersister {

    private static final Logger LOG = LoggerFactory.getLogger(EntityPersister.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveInNewTransaction(@Nonnull final Persistable<?> entity) {
        saveAndFlush(entity);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveInNewTransaction(@Nonnull final Iterable<? extends Persistable<?>> entities) {
        saveAndFlush(entities);
    }

    @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public void saveInCurrentlyOpenTransaction(@Nonnull final Persistable<?> entity) {
        saveAndFlush(entity);
    }

    @Transactional(propagation = Propagation.MANDATORY, noRollbackFor = RuntimeException.class)
    public void saveInCurrentlyOpenTransaction(@Nonnull final Iterable<? extends Persistable<?>> entities) {
        saveAndFlush(entities);
    }

    private void saveAndFlush(final Persistable<?> entity) {
        save(Objects.requireNonNull(entity));
        entityManager.flush();
        LOG.debug("Inserted {} entity with ID {} into database.", entity.getClass().getSimpleName(), entity.getId());
    }

    private void saveAndFlush(final Iterable<? extends Persistable<?>> entities) {
        int count = 0;
        for (final Persistable<?> entity : Objects.requireNonNull(entities)) {
            save(entity);
            count++;
        }
        if (count > 0) {
            entityManager.flush();
        }
        LOG.debug("Inserted {} entities into database.", count);
    }

    private void save(final Persistable<?> entity) {
        try {
            entityManager.persist(entity);
        } catch (final ConstraintViolationException cve) {
            final String joinedContraintViolations = cve.getConstraintViolations().stream()
                    .map(Functions.CONSTRAINT_VIOLATION_TO_STRING)
                    .collect(joining("\n"));
            final String msg = String.format("JSR-303 constraint violations:%n%s", joinedContraintViolations);
            throw new ConstraintViolationException(msg, cve.getConstraintViolations());
        }
    }
}
