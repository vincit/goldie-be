package fi.vincit.test;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.concurrent.Callable;

@Transactional(propagation = Propagation.REQUIRES_NEW)
@Component
public class TransactionalTaskExecutorImpl implements TransactionalTaskExecutor {

    @Override
    public void execute(@Nonnull final Runnable task) {
        task.run();
    }

    @Override
    public <T> T execute(@Nonnull final Callable<T> callable) {
        try {
            return callable.call();
        } catch (final RuntimeException e) {
            // Do not wrap exceptions when not necessary.
            // Easier for tests with @Test(expected = ...)
            throw e;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

}
