/*
 * Copyright 2012-2016 Jarkko Kaura
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fi.vincit.util;

import com.google.common.base.Joiner;
import fi.vincit.feature.common.BaseEntity;
import fi.vincit.feature.common.BaseEntityDTO;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class Asserts {

    private static final String DEFAULT_SEPARATOR = "\n    ";

    private Asserts() {
        throw new AssertionError();
    }

    public static <T> void assertEmpty(final Stream<T> stream, final String message) {
        assertEmpty(stream.collect(toList()), message);
    }

    public static void assertEmpty(final Iterable<?> iterable) {
        assertFalse(iterable.iterator().hasNext());
    }

    public static void assertEmpty(final Iterable<?> iterable, final String message) {
        assertFalse(consructErrorMessage(message, iterable, DEFAULT_SEPARATOR), iterable.iterator().hasNext());
    }

    public static <T> void assertEmpty(
            final Iterable<? extends T> iterable, final Function<? super T, String> transformer, final String message) {

        assertFalse(
                consructErrorMessage(message, iterable, transformer, DEFAULT_SEPARATOR), iterable.iterator().hasNext());
    }

    public static void assertRevision(
            @Nonnull final BaseEntityDTO<? extends Serializable> dto, @Nullable final Integer expectedRevision) {

        Objects.requireNonNull(dto, "dto must not be null");

        assertEquals(expectedRevision, dto.getRev());
    }

    public static <T extends BaseEntityDTO<? extends Serializable>> void assertRevision(
            @Nonnull final Iterable<T> dtos, final Integer expectedRevision) {

        assertTrue(StreamSupport.stream(dtos.spliterator(), false)
                .allMatch(dto -> dto != null && Objects.equals(dto.getRev(), expectedRevision)));
    }

    public static <T extends BaseEntity<? extends Serializable>> void assertEntityRevision(
            @Nonnull final Iterable<T> entities, final Integer expectedRevision) {

        Objects.requireNonNull(entities, "entities must not be null");

        assertTrue(StreamSupport.stream(entities.spliterator(), false)
                .allMatch(entity -> Objects.equals(entity.getConsistencyVersion(), expectedRevision)));
    }

    private static String consructErrorMessage(
            final String message, final Iterable<?> failedItems, final String separator) {

        return consructErrorMessage(message, join(failedItems, separator), separator);
    }

    private static <T> String consructErrorMessage(
            final String message,
            final Iterable<? extends T> failedItems,
            final Function<? super T, String> transformer,
            final String separator) {

        return consructErrorMessage(message, join(failedItems, transformer, separator), separator);
    }

    private static String consructErrorMessage(final String message, final String joinedItems, final String separator) {
        return new StringBuilder(message)
                .append(separator)
                .append(joinedItems)
                .append('\n')
                .toString();
    }

    private static String join(final Iterable<?> parts, final String separator) {
        return Joiner.on(separator).skipNulls().join(parts);
    }

    private static <T> String join(
            final Iterable<? extends T> parts, final Function<? super T, String> transformer, final String separator) {

        return Joiner.on(separator).skipNulls()
                .join(StreamSupport.stream(parts.spliterator(), false).map(transformer).collect(toList()));
    }

}