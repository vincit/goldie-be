/*
 * Copyright 2012-2016 Jarkko Kaura
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package fi.vincit.conventions;

import com.google.common.collect.ImmutableSet;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static fi.vincit.util.Asserts.assertEmpty;

public class CodeStyleTest {

    private static final Set<String> CHECKED_FILE_EXTENSIONS = ImmutableSet.of(
            "java", "xml", "xsd", "properties", "sql", "jsp", "tag", "tld", "hbs", "vm", "html", "css", "less", "scss");

    private static final Set<String> EXCLUDED_FILES = ImmutableSet.of();

    private static boolean excluded(String filename) {
        return filename.endsWith("_.java");
    }

    @Test
    public void verifySourceFilesDoNotContainTabCharacters() throws IOException {
        Iterator<File> iter = FileUtils.iterateFiles(
                new File("src"),
                new AbstractFileFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        String lcName = name.toLowerCase();
                        return !EXCLUDED_FILES.contains(name)
                                && CHECKED_FILE_EXTENSIONS.contains(Files.getFileExtension(lcName))
                                && !excluded(lcName);
                    }
                },
                TrueFileFilter.INSTANCE
        );

        final List<String> filesContainingTabs = new ArrayList<>();

        while (iter.hasNext()) {
            final File file = iter.next();
            Files.readLines(file, StandardCharsets.US_ASCII, new LineProcessor<Void>() {
                @Override
                public boolean processLine(String line) throws IOException {
                    boolean result = true;
                    if (line.contains("\t")) {
                        filesContainingTabs.add(file.getCanonicalPath());
                        result = false;
                    }
                    return result;
                }

                @Override
                public Void getResult() {
                    return null;
                }
            });
        }

        assertEmpty(filesContainingTabs, "The following files contain tab characters: ");
    }

}