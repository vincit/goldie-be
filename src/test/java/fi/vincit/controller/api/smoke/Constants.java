package fi.vincit.controller.api.smoke;

class Constants {
    public static final String API_PREFIX = "/api/test/";
    public static final String API_URL_HELLO = API_PREFIX + "hello";
    public static final String API_URL_HELLO_SECURE = API_PREFIX + "secure/hello";
    public static final String API_URL_HELLO_JSON = API_PREFIX + "helloJSON";
    public static final String API_URL_HELLO_POST = API_PREFIX + "helloPOST";
    public static final String API_URL_THROW = API_PREFIX + "throw";
}
