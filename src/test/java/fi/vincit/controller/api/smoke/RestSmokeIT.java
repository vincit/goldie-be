package fi.vincit.controller.api.smoke;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.parsing.Parser;
import fi.vincit.controller.api.AbstractRestApiIT;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static com.jayway.restassured.RestAssured.given;

public class RestSmokeIT extends AbstractRestApiIT {

    @Test
    public void testHelloString() {
        given().auth().none()
                .expect().statusCode(200)
                .body(Matchers.containsString("Hello World!"))
                .when().get(Constants.API_URL_HELLO);
    }

    @Test
    public void testHelloJSON() {
        given().header("Accept", "application/json")
                .auth().none()
                .expect().statusCode(200)
                .body("status", Matchers.equalTo("SUCCESS"))
                .body("result", Matchers.equalTo("Hello World!"))
                .when().get(Constants.API_URL_HELLO_JSON);
    }

    @Test
    public void testApiPostRequest() {
        given().contentType(ContentType.TEXT)
                .body("Test")
                .expect().defaultParser(Parser.TEXT)
                .statusCode(HttpStatus.OK.value())
                .contentType(ContentType.TEXT)
                .content(Matchers.equalTo("Hello Test!"))
                .when().post(Constants.API_URL_HELLO_POST);
    }
}
