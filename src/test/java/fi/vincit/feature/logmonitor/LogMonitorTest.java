
package fi.vincit.feature.logmonitor;

import com.google.common.collect.ImmutableMap;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogMonitorTest {
    
    private static final Logger LOG = LoggerFactory.getLogger(LogMonitorTest.class);

    private static final ZonedDateTime TIME = ZonedDateTime.of(2016, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault());

    @Test
    public void test() {
        createHistoryEntries();

        // Is new and goes straight to top.
        final int count = LogMonitor.MIN_LOGGING_COUNT + 1;
        for (int i = 0; i < count; i++) {
            LogMonitor.addLoggingEvent(TIME.toInstant().toEpochMilli(), "f");
        }

        // A lot of time has passed which is not normal, but it doesn't matter.
        List<LogMonitorResultData> results = LogMonitor.getAlarmingLoggerNamesAndCounts();
        Optional<LogMonitorResultData> result = results.stream().filter(r -> r.name.equals("f")).findFirst();
        assertTrue(result.isPresent());
        assertEquals(count, result.get().count);
    }

    @Test
    public void testCombine() {
        Map<String, Integer> entry1 = ImmutableMap.of(
            "logger1", 2,
            "logger2", 1);
        Map<String, Integer> entry2 = ImmutableMap.of(
            "logger1", 3);

        Map<String, Integer> result = LogMonitor.combine(Arrays.asList(entry1, entry2));
        assertEquals(5, (int)result.get("logger1"));
        assertEquals(1, (int)result.get("logger2"));
    }

    @Test
    public void testGetSortedLoggerNames() {
        Map<String, Integer> entry = ImmutableMap.of(
            "logger1", 2,
            "logger2", 1,
            "logger3", 5,
            "logger4", 3,
            "logger5", 10
        );

        List<String> names = LogMonitor.getLoggerNamesSortedByCount(entry);
        assertEquals(Arrays.asList("logger5", "logger3", "logger4", "logger1", "logger2"), names);
    }

    private void createHistoryEntries() {
        for (int i = 1; i <= LogMonitor.HISTORY_WINDOW_SIZE; i++) {
            for (int j = 0; j < LogMonitor.LOGGER_NAMES_TO_CHECK; j++) {
                for (int k = 0; k < LogMonitor.MIN_LOGGING_COUNT; k++) {
                    LogMonitor.addLoggingEvent(TIME.minusHours(i).toInstant().toEpochMilli(), "logger" + j);
                }
            }
        }
    }

    @Test
    public void testNoDeadlock() throws InterruptedException {
        LOG.info("This is initial required log entry");
        Thread thread = new Thread(() -> {
            IntStream.range(0, 10000).forEach(i -> {
                // See appender configuration in logback-test.xml. Without it this is meaningless.
                LOG.info("More logging {}", i);
            });
        });
        thread.start();
        IntStream.range(0, 100).forEach(i -> {
            LogMonitor.getAlarmingLoggerNamesAndCounts();
        });
        assertTrue("Logging thread has already finished. Increase iteration count inside thread to fix this test.", thread.isAlive());

        // No deadlock, success!
    }

}