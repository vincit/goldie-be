package fi.vincit.feature.mail;

import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class MailServiceSendTest extends BaseMailServiceTest {
    @Test
    public void testSendImmediate() {
        // WHEN
        final MailMessageDTO.Builder builder = createTestMessageBuilder();
        final MailMessageDTO messageDTO = mailService.sendImmediate(builder);

        // THEN
        verify(mailDeliveryService, times(1)).send(eq(messageDTO));
        verifyNoMoreInteractions(mailDeliveryService);
        verifyNoMoreInteractions(outgoingMailProvider);
    }

    @Test
    public void testSendImmediateWithoutFrom() {
        // WHEN
        final MailMessageDTO.Builder builder = createTestMessageBuilder();
        final MailMessageDTO messageDTO = mailService.sendImmediate(builder);

        // THEN
        assertThat(messageDTO.getFrom()).isEqualTo("root@example.org");
    }

    public void testSendImmediateWithFrom() {
        // WHEN
        final MailMessageDTO.Builder builder = createTestMessageBuilder()
                .withFrom("other@example.org");
        final MailMessageDTO messageDTO = mailService.sendImmediate(builder);

        // THEN
        assertThat(messageDTO.getFrom()).isEqualTo("other@example.org");
    }

    @Test
    public void testSend() {
        // WHEN
        final MailMessageDTO.Builder builder = createTestMessageBuilder();
        final MailMessageDTO messageDTO = mailService.send(builder);

        // THEN
        verify(outgoingMailProvider, times(1)).scheduleForDelivery(eq(messageDTO), any(Optional.class));
        verifyNoMoreInteractions(mailDeliveryService);
        verifyNoMoreInteractions(outgoingMailProvider);
    }

    @Test
    public void testSendLater() {
        // GIVEN
        final Instant sendAfterTime = OffsetDateTime.now().plusDays(1).toInstant();

        // WHEN
        final MailMessageDTO.Builder builder = createTestMessageBuilder();
        final MailMessageDTO messageDTO = mailService.sendLater(builder, sendAfterTime);

        // THEN
        verify(outgoingMailProvider, times(1)).scheduleForDelivery(eq(messageDTO), eq(Optional.of(sendAfterTime)));
        verifyNoMoreInteractions(mailDeliveryService);
        verifyNoMoreInteractions(outgoingMailProvider);
    }

    @Test
    public void testWhenDeliveryDisabled() {
        // GIVEN
        ReflectionTestUtils.setField(mailService, "mailDeliveryEnabled", false);

        // WHEN
        mailService.send(createTestMessageBuilder());
        mailService.sendImmediate(createTestMessageBuilder());
        mailService.sendLater(createTestMessageBuilder(), OffsetDateTime.now().minusDays(1).toInstant());

        // THEN
        verifyNoMoreInteractions(mailDeliveryService);
        verifyNoMoreInteractions(outgoingMailProvider);
    }
}
