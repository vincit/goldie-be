package fi.vincit.feature.account.user;

import fi.vincit.security.authentication.TestAuthenticationTokenUtil;
import fi.vincit.security.authorization.EntityAuthorizationStrategy;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;
import org.springframework.test.util.ReflectionTestUtils;

import static fi.vincit.security.EntityPermission.CREATE;
import static fi.vincit.security.EntityPermission.DELETE;
import static fi.vincit.security.EntityPermission.READ;
import static fi.vincit.security.EntityPermission.UPDATE;
import static org.assertj.core.api.Assertions.assertThat;
import org.mockito.ArgumentMatchers;

public class SystemUserAuthorizationTest {
    private static final long USER_ID = 1;

    @Mock
    private RoleHierarchy roleHierarchy;

    private EntityAuthorizationStrategy<SystemUser> strategy;
    private SystemUser user;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.strategy = create();
        this.user = new SystemUser();
        this.user.setId(USER_ID);
    }

    private EntityAuthorizationStrategy<SystemUser> create() {
        final SystemUserAuthorization strategy = new SystemUserAuthorization();

        Mockito.when(roleHierarchy.getReachableGrantedAuthorities(ArgumentMatchers.anyCollection()))
                .thenAnswer((Answer<Object>) invocationOnMock -> invocationOnMock.getArguments()[0]);

        ReflectionTestUtils.setField(strategy, "roleHierarchy", roleHierarchy);

        return strategy;
    }

    private void assertNoPermission(Authentication authentication) {
        assertThat(strategy.hasPermission(user, READ, authentication)).isFalse();
        assertThat(strategy.hasPermission(user, UPDATE, authentication)).isFalse();
        assertThat(strategy.hasPermission(user, DELETE, authentication)).isFalse();
        assertThat(strategy.hasPermission(user, CREATE, authentication)).isFalse();
    }

    @Test
    public void testSimple() {
        assertThat(strategy.getEntityClass()).isEqualTo(SystemUser.class);
    }

    @Test
    public void testAuthorizeSelf() {
        final Authentication authentication = TestAuthenticationTokenUtil.createUserAuthentication(USER_ID);

        assertNoPermission(authentication);
    }

    @Test
    public void testAuthorizeAdmin() {
        final Authentication authentication = TestAuthenticationTokenUtil.createAdminAuthentication();

        assertThat(strategy.hasPermission(user, READ, authentication)).isTrue();
        assertThat(strategy.hasPermission(user, UPDATE, authentication)).isTrue();
        assertThat(strategy.hasPermission(user, CREATE, authentication)).isTrue();
        assertThat(strategy.hasPermission(user, DELETE, authentication)).isTrue();
    }
}
