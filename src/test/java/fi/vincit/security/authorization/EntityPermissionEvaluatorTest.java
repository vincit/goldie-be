package fi.vincit.security.authorization;

import fi.vincit.feature.common.BaseEntity;
import fi.vincit.security.authentication.TestAuthenticationTokenUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;

import java.util.Collections;

import static fi.vincit.security.EntityPermission.DELETE;
import static fi.vincit.security.EntityPermission.READ;
import static fi.vincit.security.EntityPermission.UPDATE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

public class EntityPermissionEvaluatorTest {

    private static class SimpleEntity extends BaseEntity<Long> {
        private Long id = 1L;

        @Override
        public Long getId() {
            return id;
        }

        @Override
        public void setId(Long id) {
            this.id = id;
        }
    }

    @Mock
    private EntityAuthorizationStrategy<SimpleEntity> authorizationStrategy;

    private EntityPermissionEvaluator evaluator;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.evaluator = createEvaluator();
    }

    private EntityPermissionEvaluator createEvaluator() {
        when(authorizationStrategy.getEntityClass()).thenReturn(SimpleEntity.class);
        when(authorizationStrategy.hasPermission(any(SimpleEntity.class),
                eq(READ), any(Authentication.class))).thenReturn(true);
        when(authorizationStrategy.hasPermission(any(SimpleEntity.class),
                eq(UPDATE), any(Authentication.class))).thenReturn(true);
        when(authorizationStrategy.hasPermission(any(SimpleEntity.class),
                eq(DELETE), any(Authentication.class))).thenReturn(false);

        return new EntityPermissionEvaluator(Collections.singletonList(authorizationStrategy));
    }

    @Test
    public void testWithEntity() {
        final SimpleEntity entity = new SimpleEntity();

        final Authentication authentication = TestAuthenticationTokenUtil.createUserAuthentication();
        assertThat(evaluator.hasPermission(authentication, entity, READ)).isTrue();
        assertThat(evaluator.hasPermission(authentication, entity, UPDATE)).isTrue();
        assertThat(evaluator.hasPermission(authentication, entity, DELETE)).isFalse();
    }
}