package fi.vincit.sql;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * SQFileMetadata is a Querydsl query type for SQFileMetadata
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class SQFileMetadata extends com.querydsl.sql.RelationalPathBase<SQFileMetadata> {

    private static final long serialVersionUID = -1636900871;

    public static final SQFileMetadata fileMetadata = new SQFileMetadata("file_metadata");

    public final NumberPath<Integer> consistencyVersion = createNumber("consistencyVersion", Integer.class);

    public final StringPath contentHash = createString("contentHash");

    public final NumberPath<Long> contentSize = createNumber("contentSize", Long.class);

    public final StringPath contentType = createString("contentType");

    public final NumberPath<Long> createdByUserId = createNumber("createdByUserId", Long.class);

    public final DateTimePath<java.sql.Timestamp> creationTime = createDateTime("creationTime", java.sql.Timestamp.class);

    public final NumberPath<Long> deletedByUserId = createNumber("deletedByUserId", Long.class);

    public final DateTimePath<java.sql.Timestamp> deletionTime = createDateTime("deletionTime", java.sql.Timestamp.class);

    public final SimplePath<Object> fileMetadataUuid = createSimple("fileMetadataUuid", Object.class);

    public final DateTimePath<java.sql.Timestamp> modificationTime = createDateTime("modificationTime", java.sql.Timestamp.class);

    public final NumberPath<Long> modifiedByUserId = createNumber("modifiedByUserId", Long.class);

    public final StringPath originalFileName = createString("originalFileName");

    public final StringPath resourceUrl = createString("resourceUrl");

    public final StringPath storageType = createString("storageType");

    public final com.querydsl.sql.PrimaryKey<SQFileMetadata> fileMetadataPkey = createPrimaryKey(fileMetadataUuid);

    public final com.querydsl.sql.ForeignKey<SQFileStorageType> fileMetadataStorageTypeFk = createForeignKey(storageType, "name");

    public final com.querydsl.sql.ForeignKey<SQFileContent> _fileContentMetadataUuidFk = createInvForeignKey(fileMetadataUuid, "file_metadata_uuid");

    public SQFileMetadata(String variable) {
        super(SQFileMetadata.class, forVariable(variable), "public", "file_metadata");
        addMetadata();
    }

    public SQFileMetadata(String variable, String schema, String table) {
        super(SQFileMetadata.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public SQFileMetadata(Path<? extends SQFileMetadata> path) {
        super(path.getType(), path.getMetadata(), "public", "file_metadata");
        addMetadata();
    }

    public SQFileMetadata(PathMetadata metadata) {
        super(SQFileMetadata.class, metadata, "public", "file_metadata");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(consistencyVersion, ColumnMetadata.named("consistency_version").withIndex(2).ofType(Types.INTEGER).withSize(10).notNull());
        addMetadata(contentHash, ColumnMetadata.named("content_hash").withIndex(13).ofType(Types.VARCHAR).withSize(255).notNull());
        addMetadata(contentSize, ColumnMetadata.named("content_size").withIndex(12).ofType(Types.BIGINT).withSize(19).notNull());
        addMetadata(contentType, ColumnMetadata.named("content_type").withIndex(11).ofType(Types.VARCHAR).withSize(255).notNull());
        addMetadata(createdByUserId, ColumnMetadata.named("created_by_user_id").withIndex(3).ofType(Types.BIGINT).withSize(19));
        addMetadata(creationTime, ColumnMetadata.named("creation_time").withIndex(6).ofType(Types.TIMESTAMP).withSize(35).withDigits(6).notNull());
        addMetadata(deletedByUserId, ColumnMetadata.named("deleted_by_user_id").withIndex(4).ofType(Types.BIGINT).withSize(19));
        addMetadata(deletionTime, ColumnMetadata.named("deletion_time").withIndex(7).ofType(Types.TIMESTAMP).withSize(35).withDigits(6));
        addMetadata(fileMetadataUuid, ColumnMetadata.named("file_metadata_uuid").withIndex(1).ofType(Types.OTHER).withSize(2147483647).notNull());
        addMetadata(modificationTime, ColumnMetadata.named("modification_time").withIndex(8).ofType(Types.TIMESTAMP).withSize(35).withDigits(6).notNull());
        addMetadata(modifiedByUserId, ColumnMetadata.named("modified_by_user_id").withIndex(5).ofType(Types.BIGINT).withSize(19));
        addMetadata(originalFileName, ColumnMetadata.named("original_file_name").withIndex(10).ofType(Types.VARCHAR).withSize(255));
        addMetadata(resourceUrl, ColumnMetadata.named("resource_url").withIndex(14).ofType(Types.VARCHAR).withSize(255));
        addMetadata(storageType, ColumnMetadata.named("storage_type").withIndex(9).ofType(Types.VARCHAR).withSize(255).notNull());
    }

}

