package fi.vincit.sql;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;

import com.querydsl.sql.ColumnMetadata;
import java.sql.Types;




/**
 * SQFileStorageType is a Querydsl query type for SQFileStorageType
 */
@Generated("com.querydsl.sql.codegen.MetaDataSerializer")
public class SQFileStorageType extends com.querydsl.sql.RelationalPathBase<SQFileStorageType> {

    private static final long serialVersionUID = 1100775243;

    public static final SQFileStorageType fileStorageType = new SQFileStorageType("file_storage_type");

    public final StringPath name = createString("name");

    public final com.querydsl.sql.PrimaryKey<SQFileStorageType> fileStorageTypePkey = createPrimaryKey(name);

    public final com.querydsl.sql.ForeignKey<SQFileMetadata> _fileMetadataStorageTypeFk = createInvForeignKey(name, "storage_type");

    public SQFileStorageType(String variable) {
        super(SQFileStorageType.class, forVariable(variable), "public", "file_storage_type");
        addMetadata();
    }

    public SQFileStorageType(String variable, String schema, String table) {
        super(SQFileStorageType.class, forVariable(variable), schema, table);
        addMetadata();
    }

    public SQFileStorageType(Path<? extends SQFileStorageType> path) {
        super(path.getType(), path.getMetadata(), "public", "file_storage_type");
        addMetadata();
    }

    public SQFileStorageType(PathMetadata metadata) {
        super(SQFileStorageType.class, metadata, "public", "file_storage_type");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(name, ColumnMetadata.named("name").withIndex(1).ofType(Types.VARCHAR).withSize(255).notNull());
    }

}

