#!/bin/bash

#
# This script executes the integration tests (RestAssured) and measures the
# test coverage on the server side (Jetty). Reports are generated to
# ./target/site/jacoco/index.html.
#

mvn clean package -Pintegration-test

export OLD_MAVEN_OPTS=$MAVEN_OPTS
export MAVEN_OPTS="$OLD_MAVEN_OPTS -javaagent:$HOME/.m2/repository/org/jacoco/org.jacoco.agent/0.7.4.201502262128/org.jacoco.agent-0.7.4.201502262128-runtime.jar=destfile=./target/coverage-reports/jacoco-it.exec"

mvn verify -Pintegration-test

export MAVEN_OPTS=$OLD_MAVEN_OPTS

mvn jacoco:report -Pintegration-test

